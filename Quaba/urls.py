from django.conf.urls import patterns, url, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()

urlpatterns = patterns('quabamv.views',

    url(r'^quaba/$', 'welcome', name='quaba'),
    
    url(r'^quaba/about/$', 'about', name='about'),

    url(r'^quaba/404/$', 'handler404', name='error_404'),
    
    url(r'^quaba/500/$', 'handler500', name='error_500'),
           
    url(r'^quaba/admin/',
        include(admin.site.urls)),
        
    url(r'^quaba/register/$', 
        'register', 
        name = 'register'),
    
    url(r'^quaba/login/$', 
        'log_in', 
        name = 'log_in'),
            
    url(r'^quaba/recovery/$', 
        'password_recovery', 
        name = 'recovery'),  

    url(r'^quaba/main/$', 
        'main_menu',
        name = 'main_menu'),
                    
    url(r'^quaba/main/logout/$', 
        'log_out', 
        name = 'log_out'),
                
    url(r'^quaba/main/detail/$', 
        'profile_detail',
        name='profile_detail'),
        
    url(r'^quaba/main/settings/$', 
        'settings_menu', 
        name = 'settings_menu'),
        
    url(r'^quaba/main/settings/edit_profile/$',
        'edit_profile',
        name='edit_profile'),
        
    url(r'^quaba/main/settings/password_modify/$', 
        'password_modify', 
        name = 'password_modify'),
        
    url(r'^quaba/main/settings/sign_down/$',
        'sign_down',
        name='sign_down'),
                
    #####
    
    url(r'^quaba/main/friends/$', 
        'friends_menu', 
        name = 'friends_menu'),
        
    url(r'^quaba/main/friends/search/$',
        'search_user', 
        name='search_any_user'),
        
    url(r'^quaba/main/friends/search/results/$',
        'search_user'),
               
    ###
                
    url(r'^quaba/main/friends/messages/inbox/$', 
        'inbox', 
        name='messages_inbox'),

    url(r'^quaba/main/friends/messages/outbox/$', 
        'outbox', 
        name='messages_outbox'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/messages/$', 
        'friend_messages', 
        name='messages_friendbox'),
        
    url(r'^quaba/main/friends/messages/compose/(?P<email>[^/]+)/$', 
        'compose', 
        name='messages_compose'),
        
    url(r'^quaba/main/friends/messages/message/(?P<message_id>[\d]+)/$', 
        'detail', 
        name='messages_detail'),
        
    url(r'^quaba/main/friends/messages/message/(?P<message_id>[\d]+)/report/$', 
        'message_report', 
        name='messages_report'),
        
    url(r'^quaba/main/friends/messages/message/(?P<message_id>[\d]+)/delete/$', 
        'delete', 
        name='messages_delete'),

    url(r'^quaba/main/friends/messages/message/(?P<message_id>[\d]+)/reply/$', 
        'reply', 
        name='messages_reply'),
                
  
    ###
        
    url(r'^quaba/main/friends/list/$',
        'list_friends',
        name='list_friends'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/$',
        'user_view', 
        name='user_view'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/report/$',
        'user_report', 
        name='report_user'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/block/$',
        'block_user', 
        name='block_user'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/detail/$', 
        'friend_profile_detail',
        name='friend_profile_detail'),
        
    url(r'^quaba/main/friends/list/search/$',
        'search_friend', 
        name='search_friend'),
        
    url(r'^quaba/main/friends/list/search/results/$',
        'search_user'),
       
    url(r'^quaba/main/friends/invite/(?P<email>[^/]+)/$',
       'invite_friend',
       name='invite_friend'),

    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/delete/$',
       'remove_friend',
       name='remove_friend'),
       
    url(r'^quaba/main/friends/blocked/$',
       'list_blocked_users',
       name='list_blocked_users'),

    url(r'^quaba/main/friends/blocked/unblock/(?P<email>[^/]+)/$',
       'unblock_user',
       name='unblock_user'),
       
    ###
    

    url(r'^quaba/main/friends/invitations/received/$',
       'list_friend_received_invitations',
       name='friends_received_invitations'),

    url(r'^quaba/main/friends/invitations/sent/$',
       'list_friend_sent_invitations',
       name='friends_sent_invitations'),
       
    url(r'^quaba/main/friends/invitations/invitation/(?P<invitation_id>[\d]+)/delete/$',
       'delete_friend_invitation',
       name='friends_delete_invitation'),

    url(r'^quaba/main/friends/invitations/invitation/(?P<invitation_id>[\d]+)/respond/$',
       'respond_to_friend_invitation',
       name='friends_respond_invitation'),
       
    ###

    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/friends/$',
        'list_friend_friends',
        name='list_friend_friends'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/friends/search/$',
        'search_friend_of_friend', 
        name='search_friend_of_friend'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/friends/search/results/$',
        'search_user'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/groups/search/$',
        'search_friend_group', 
        name='search_friend_group'),
        
    url(r'^quaba/main/friends/groups/(?P<email>[^/]+)/groups/search/results/$',
        'search_group'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/groups/$',
        'list_friend_groups',
        name='list_friend_groups'),
               
    #####
    
    url(r'^quaba/main/groups/$',
        'groups_menu',
        name='groups_menu'),
        
    url(r'^quaba/main/groups/list/$',
        'list_groups',
        name='list_groups'),
        
    url(r'^quaba/main/groups/search/$',
        'search_group', 
        name='search_group'),
                
    url(r'^quaba/main/groups/search/results/$',
        'search_group'),
        
    url(r'^quaba/main/groups/list/search/$',
        'search_user_group',
        name='search_user_group'),
        
    url(r'^quaba/main/groups/list/search/results/$',
        'search_group'),
        
    url(r'^quaba/main/groups/create_group/$',
        'create_group',
        name='create_group'),
    
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/$',
        'group_view',
        name='group_view'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/rename/$',
        'rename_group',
        name='rename_group'), 
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/new_photo/$',
        'group_main_photo',
        name='group_main_photo'), 
     
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/delete/$',
        'delete_group',
        name='delete_group'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/report/$',
        'group_report',
        name='report_group'),

    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/send_request/$',
        'group_send_request',
        name='send_request'),
               
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/requests/$',
       'list_group_received_requests',
       name='groups_received_requests'),
       
    url(r'^quaba/main/groups/requests/$',
       'list_group_sent_requests',
       name='groups_sent_requests'),
       
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/request/(?P<email>[^/]+)/respond/$',
        'respond_to_group_request',
        name='groups_respond_request'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/delete_request/$',
        'delete_group_request',
        name='groups_delete_request'),
        
    ###

    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/members/$',
        'list_group_members',
        name='list_group_members'), 
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/members/search/results/$',
        'search_group_member',
        name='search_group_member'), 
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/invite/$',
        'invite_to_group',
        name='invite_to_group'),

    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/comments/new_comment/$',
        'group_new_comment',
        name='group_new_comment'),      
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/remove/$',
        'group_remove_comment',
        name='group_remove_comment'), 
 
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/like/$',
        'group_like_comment',
        name='group_like_comment'),  
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/likelist/$',
        'group_comment_likes_list',
        name='group_comment_likes_list'),  

    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/leave/$',
        'leave_group',
        name='leave_group'),
    
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/remove/(?P<email>[^/]+)/$',
        'remove_from_group',
        name='remove_from_group'),    
        
    ###
        
    url(r'^quaba/main/groups/invitations/received/$',
       'list_member_received_invitations',
       name='members_received_invitations'),

    url(r'^quaba/main/groups/invitations/sent/$',
       'list_member_sent_invitations',
       name='members_sent_invitations'),
       
    ###

    url(r'^quaba/main/groups/group/(?P<group_id>[\.\w]+)/invitations/received/$',
       'list_group_received_invitations',
       name='groups_received_invitations'),

    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/invitations/sent/$',
       'list_group_sent_invitations',
       name='groups_sent_invitations'),
       
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/invitation/(?P<invitation_id>[\d]+)/delete/$',
       'delete_group_invitation',
       name='groups_delete_invitation'),

    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/invitation/(?P<invitation_id>[\d]+)/respond/$',
       'respond_to_group_invitation',
       name='groups_respond_invitation'),
       
    #####
    
    url(r'^quaba/main/photos/$',
        'photos_menu',
        name='photos_menu'),
               
    url(r'^quaba/main/photos/photobook/$',
        'photobook_view',
        name='photobook_view'),
        
    url(r'^quaba/main/photos/photobook/search/$',
        'filter_photos', 
        name='filter_photos'),
        
    url(r'^quaba/main/photos/photobook/search/results/$',
        'filter_photos'),
        
    url(r'^quaba/main/photos/photobook/upload_photo/$',
        'upload_photo',
        name='upload_photo'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/$',
       'friend_photobook_view',
       name='friend_photobook_view'),
       
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/search/$',
        'friend_filter_photos', 
        name='friend_filter_photos'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/search/results/$',
        'friend_filter_photos'),
                 
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/$',
        'photo_view',
        name='photo_view'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/$',
        'friend_photo_view',
        name='friend_photo_view'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/report/$',
        'photo_report',
        name='report_photo'),
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/main/$',
        'user_main_photo',
        name='profile_photo'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/main/$',
        'user_main_photo',
        name='friend_profile_photo'),
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/rename/$',
        'rename_photo',
        name='rename_photo'),
              
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/rename/$',
        'rename_photo',
        name='friend_rename_photo'),
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/delete/$',
        'delete_photo',
        name='delete_photo'),
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/shdelete/$',
        'delete_shared_photo',
        name='delete_shared_photo'),
                
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/delete/$',
        'delete_photo',
        name='friend_delete_photo'),
                
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/share/$',
        'share_photo',
        name='share_photo'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/share/$',
        'share_photo',
        name='friend_share_photo'),
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/like/$',
        'like_photo',
        name='like_photo'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/like/$',
        'like_photo',
        name='friend_like_photo'),
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/likelist/$',
        'photo_likes_list',
        name='photo_likes_list'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/likelist/$',
        'photo_likes_list',
        name='friend_photo_likes_list'),
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/comments/new_comment/$',
        'photo_new_comment',
        name='photo_new_comment'),  
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/new_comment/$',
        'photo_new_comment',
        name='friend_photo_new_comment'),
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/remove/$',
        'photo_delete_comment',
        name='photo_delete_comment'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/remove/$',
        'photo_delete_comment',
        name='friend_photo_delete_comment'),
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/like/$',
        'photo_like_comment',
        name='photo_like_comment'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/like/$',
        'photo_like_comment',
        name='friend_photo_like_comment'),  
        
    url(r'^quaba/main/photos/photobook/photo/(?P<photo_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/likelist/$',
        'photo_comment_likes_list',
        name='photo_comment_likes_list'),
        
    url(r'^quaba/main/friends/friend/(?P<email>[^/]+)/photos/photo/(?P<photo_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/likelist/$',
        'photo_comment_likes_list',
        name='friend_photo_comment_likes_list'),
         
    ###
    
    url(r'^quaba/main/storage/$',
        'store_view',
        name='store_view'),
        
    url(r'^quaba/main/storage/search/$',
        'search_in_store',
        name='store_search'),
        
    url(r'^quaba/main/storage/search/results/$',
        'search_in_store'),
                
    url(r'^quaba/main/storage/upload_file/$',
        'upload_file',
        name='upload_file'),
            
    url(r'^quaba/main/storage/file/(?P<file_id>[\d]+)/delete/$',
        'delete_file',
        name='delete_file'),
        
    url(r'^quaba/main/storage/file/(?P<file_id>[\d]+)/share/$',
        'share_file',
        name='share_file'),
        
    url(r'^quaba/main/storage/file/(?P<file_id>[\d]+)/report/$',
        'file_report',
        name='report_file'),
        
    url(r'^quaba/main/storage/file/(?P<file_id>[\d]+)/download/$',
        'download_file',
        name='download_file'),
        
    url(r'^quaba/main/storage/create_folder/$',
        'create_folder',
        name='create_folder'),
        
    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)/$',
        'folder_view',
        name='folder_view'),
        
    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)/search/$',
        'search_in_folder',
        name='folder_search'),
        
    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)/search/results/$',
        'search_in_folder'),
                
    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)/rename/$',
        'rename_folder',
        name='rename_folder'),
        
    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)?/upload_file/$',
        'upload_file_in_folder',
        name='upload_file_in_folder'),
        
    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)/delete/$',
        'delete_folder',
        name='delete_folder'),
        
    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)/create_folder/$',
        'create_folder_in_folder',
        name='create_folder_in_folder'),
        
    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)/report/$',
        'folder_report',
        name='report_folder'),

    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)/share/$',
        'share_folder',
        name='share_folder'),
             
    url(r'^quaba/main/storage/folder/(?P<folder_id>[\d]+)/download/$',
        'download_folder',
        name='download_folder'),
        
    url(r'^quaba/main/storage/folder/(?P<id>[\d]+)/move/$',
        'move_into_folder',
        {'oid': 1},
        name='move_folder_into_folder'),
        
    url(r'^quaba/main/storage/file/(?P<id>[\d]+)/move/$',
        'move_into_folder',
        {'oid': 2},
        name='move_file_into_folder'),
        
    url(r'^quaba/main/storage/folder/(?P<id>[\d]+)/remove/$',
        'move_out_of_folder',
        {'oid': 1},
        name='move_folder_out_of_folder'),
        
    url(r'^quaba/main/storage/folder/(?P<id>[\d]+)/remove/$',
        'move_out_of_folder',
        {'oid': 2},
        name='move_file_out_of_folder'),
                
    #####
                   
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/$',
        'group_photobook_view',
        name='group_photobook_view'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/search/$',
        'group_filter_photos', 
        name='group_filter_photos'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/search/results/$',
        'group_filter_photos'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/upload_photo/$',
        'group_upload_photo',
        name='group_upload_photo'),
               
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/$',
        'group_photo_view',
        name='group_photo_view'),
                
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/rename/$',
        'group_rename_photo',
        name='group_rename_photo'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/delete/$',
        'group_delete_photo',
        name='group_delete_photo'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/main/$',
        'group_main_photo',
        name='group_main_photo'),
             
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/like/$',
        'group_like_photo',
        name='group_like_photo'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/likelist/$',
        'group_photo_likes_list',
        name='group_photo_likes_list'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/comments/new_comment/$',
        'group_photo_new_comment',
        name='group_photo_new_comment'),  
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/omments/comment/(?P<comment_id>[\d]+)/remove/$',
        'group_photo_delete_comment',
        name='group_photo_delete_comment'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/like/$',
        'group_photo_like_comment',
        name='group_photo_like_comment'),
        
    url(r'^quaba/main/groups/group/(?P<group_id>[\d]+)/photos/photo/(?P<photo_id>[\d]+)/comments/comment/(?P<comment_id>[\d]+)/likelist/$',
        'group_photo_comment_likes_list',
        name='group_photo_comment_likes_list'),
                
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
