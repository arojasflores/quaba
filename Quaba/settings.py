"""
Django settings for Quaba project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.conf import settings

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'injam20-63f4q4jn#c!)xa*ex*ladk-rvd3$z8p_zuf&o#8%*l'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

ALLOWED_HOSTS = []
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
    )

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'quabamv',
    'south',
)
                                           
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'Quaba.urls'
WSGI_APPLICATION = 'Quaba.wsgi.application'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'quabamv.context_processors.notification_type',
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'USER': 'QHOST',
        'PASSWORD': 'QHOST123456',
    }
}
                                   
                                       
# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'Europe/Madrid'

USE_I18N = True

USE_L10N = True

USE_TZ = False

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = '/quabaPFC/quabamv/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'qbmedia')

MEDIA_URL = '/qbmedia/'

DEFAULT_FROM_EMAIL = 'staff@quaba.com'

SERVER_EMAIL = 'staff@quaba.com'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = 'smtp.gmail.com'

EMAIL_HOST_USER = 'quabastaff@gmail.com'

EMAIL_HOST_PASSWORD = 'qbp455w0rd'

EMAIL_PORT = 587

EMAIL_USE_TLS = True

#notifications

VIEW_USER = 1
VIEW_MESSAGES = 2
VIEW_USER_PHOTO = 3
VIEW_USER_INVITATION = 4
VIEW_USER_FOLDER = 5
VIEW_STORE = 6

VIEW_GROUP = 11
VIEW_GROUP_MEMBERS = 12
VIEW_GROUP_REQUEST = 13
VIEW_GROUP_INVITATION = 14
VIEW_GROUP_PHOTO = 15
