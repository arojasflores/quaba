# -*- coding: utf-8 -*-

from django import forms
from django.conf import settings
from django.utils import timezone
from datetime import date
from django.utils.dates import MONTHS
from django.core.validators import ValidationError
from django.contrib.admin.widgets import AdminDateWidget

from models import *

class UserProfileForm(forms.Form):  
    
    MALE = 'M'
    FEMALE = 'F'
    
    SEX = ((MALE, "Hombre"),(FEMALE, "Mujer"),)
    
    email = forms.EmailField(label='Email', max_length=50)
    dni = forms.CharField(label='DNI/NIE/NIF', max_length=9)
    first_name = forms.CharField(label='Nombre', max_length=50)
    last_name = forms.CharField(label='Apellidos', max_length=75)
    birth_date = forms.DateField(label='Fecha de nacimiento (dd/mm/aaaa)', widget=forms.DateTimeInput(format='%d/%m/%Y'))
    gender = forms.ChoiceField(widget=forms.Select(), choices=SEX, initial= MALE, label='Sexo')
    password = forms.CharField(label='Contraseña', max_length=16, widget=forms.PasswordInput())
    password_repeat = forms.CharField(label='Contraseña (repetir)', max_length=16, widget=forms.PasswordInput())
    
    def clean_DNI(self):
        tabla = "TRWAGMYFPDXBNJZSQVHLCKE"
        dig_ext = "XYZ"
        reemp_dig_ext = {'X':'0', 'Y':'1', 'Z':'2'}
        numeros = "1234567890"
        dni = self.cleaned_data['dni'].upper()
        if len(dni) < 9:
            raise ValidationError('Introduzca un DNI/NIE/NIF de al menos 9 caracteres')
        dni = dni[:8]
        if dni[0] in dig_ext:
            dni = dni.replace(dni[0], reemp_dig_ext[dni[0]])
        if len(dni) != len([n for n in dni if n in numeros]):
            raise ValidationError('DNI/NIE/NIF Invalido')
        if tabla[int(dni)%23] != dni[8]:
            raise ValidationError('DNI/NIE/NIF Invalido')
        return dni
    
    def clean(self):       
        password = self.cleaned_data['password'] 
        password_repeat = self.cleaned_data['password_repeat']
        
        first_name_length = len(self.cleaned_data['first_name'])
        last_name_length = len(self.cleaned_data['last_name'])
        email_length = len(self.cleaned_data['email'])
        pwd_length = len(password)
        pwd_rp_length = len(password_repeat)
        age = (date.today()-self.cleaned_data['birth_date']).days/365
        
        if not first_name_length > 0:
            raise ValidationError("Por favor, introduzca un nombre valido")
        if not last_name_length > 0:
            raise ValidationError("Por favor, introduzca apellido(s) valido(s)")
        if not email_length > 0:
            raise ValidationError("Por favor, introduzca un email valido")     
        if password != password_repeat:
            raise ValidationError("Las contraseñas no coinciden")
        if pwd_length < 8 or pwd_rp_length < 8:
            raise ValidationError("Por favor, introduzca una contraseña con al menos 8 caracteres")
        if age < 16:
            raise ValidationError("Debe tener al menos 16 anos para poder crear una cuenta en Quaba")
        return self.cleaned_data
        
    def save(self):
        age = (date.today()-self.cleaned_data['birth_date']).days/365
        user = UserProfile(first_name=self.cleaned_data['first_name'],
                           last_name=self.cleaned_data['last_name'],
                           email=self.cleaned_data['email'],
                           dni=self.cleaned_data['dni'],
                           password=self.cleaned_data['password'],
                           gender = self.cleaned_data['gender'],
                           age=age)
        user.save()        
        return user
                                      
class UserNewPasswordForm(forms.Form):
    
    def __init__(self, user, *args, **kwargs):
        super(UserNewPasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password'] = forms.CharField(label="Introduce tu nueva contraseña", 
                                                        max_length=16, 
                                                        widget=forms.PasswordInput())
        self.fields['new_password_repeat'] = forms.CharField(label="Vuelve a introducir tu nueva contraseña", 
                                                                max_length=16, 
                                                                widget=forms.PasswordInput())
        self.user = user
    
    def clean(self):
        npassword = self.cleaned_data['new_password']
        npassword_repeat = self.cleaned_data['new_password_repeat'] 
        npwd_length = len(npassword)
        npwd_rp_length = len(npassword_repeat) 
        if npwd_length < 8 or npwd_rp_length < 8:
            raise ValidationError("Por favor, introduzca una contraseña con al menos 8 caracteres")
        if npassword != npassword_repeat:
            raise ValidationError("Las contraseñas no coinciden")
        if self.user.password == npassword:
            raise ValidationError("Por favor, introduzca una contraseña distinta a la actual")   
        return self.cleaned_data

class AdditionalInfoForm(forms.Form):

    def __init__(self, user, *args, **kwargs):
        super(AdditionalInfoForm, self).__init__(*args, **kwargs)
        self.fields['country'] = forms.CharField(label="Introduce el nombre de tu pais", max_length=50, 
                                                    required=False)
        self.fields['town'] = forms.CharField(label="Introduce el nombre de tu ciudad", max_length=75, 
                                                    required=False) 
        self.fields['network'] = forms.CharField(label="Introduce el nombre de tu empresa actual", max_length=50, 
                                                    required=False)
        self.fields['phone_visible'] = forms.BooleanField(label="¿Quieres que tu teléfono móvil sea visible?", required=False)
        self.fields['phone'] = forms.IntegerField(label="Introduce tu teléfono móvil", 
                                                    required=False)
        self.user = user
        photobook,opx = UserPhotoBook.objects.get_or_create(user=user)
        user_photos = UserPhoto.objects.filter(photo_book=photobook)
        self.fields['profile_photo'] = forms.ModelChoiceField(queryset=user_photos, 
                                                                label="Selecciona una foto para tu perfil", 
                                                                required=False)

    def save(self):
        userinfo,ui = UserAdditionalInfo.objects.get_or_create(user=self.user)
        userinfo.country = self.cleaned_data['country']
        userinfo.town = self.cleaned_data['town']
        userinfo.network = self.cleaned_data['network']
        userinfo.phone_visible = self.cleaned_data['phone_visible']
        userinfo.phone = self.cleaned_data['phone']
        userinfo.profile_photo = self.cleaned_data['profile_photo']
        userinfo.save()
        return userinfo
        
class MessageForm(forms.Form):
    body = forms.CharField(label="Introduce tu mensaje", max_length=500, widget=forms.Textarea)
                
    def clean_body(self):
        body = self.cleaned_data['body']
        if len(body) == 0:
            raise ValidationError('No puedes enviar un mensaje vacio')
        return body
        
    def save(self, send_from, send_to, parent_msg=None):
        if parent_msg:
            message = Message(send_from = send_from,
                                send_to = send_to,
                                body = self.cleaned_data['body'],
                                parent_msg = parent_msg,
                                replied = True)
        else:
            message = Message(send_from = send_from,
                                send_to = send_to,
                                body = self.cleaned_data['body'])  
        message.save()
        return message
        
#####

class FriendsGroupForm(forms.Form):
    name = forms.CharField(label='Introduce el nombre de tu grupo', max_length=50)

    def clean(self):
        name = self.cleaned_data["name"]
        if not len(name) > 0:
            raise forms.ValidationError("Nombre de grupo invalido")
        return self.cleaned_data
        
    def save(self, user):
        group = FriendsGroup(name=self.cleaned_data["name"], creator=user)
        group.save()
        return group
        
class GroupSelectPhotoForm(forms.Form):

    def __init__(self, group, *args, **kwargs):
        super(FriendsGroupForm, self).__init__(*args, **kwargs)
        self.group = group
        photobook, px = GroupPhotoBook.objects.get_or_create(group=group)
        group_photos = GroupPhoto.objects.filter(photo_book=photobook)
        self.fields['group_photo'] = forms.ModelChoiceField(queryset=group_photos, 
                                                            label="Selecciona una foto para tu perfil", 
                                                            required=False)
                
    def save(self):
        photo = GroupMainPhoto(group=self.group, photo=self.cleaned_data['group_photo'])
        photo.save()
        return photo
        
                
class InviteGroupForm(forms.Form):
 
    def __init__(self, user, group, friend_query, *args, **kwargs):
        super(InviteGroupForm, self).__init__(*args, **kwargs)
        self.group = group
        self.user = user
        self.fields['to_users'] = forms.ModelMultipleChoiceField(
                                                            queryset=friend_query.order_by('last_name'), 
                                                            widget=forms.CheckboxSelectMultiple(),
                                                            label='Selecciona uno o varios amigos')

    def clean_to_users(self):
        to_users = self.cleaned_data['to_users']
        if len(to_users) == 0:
            raise forms.ValidationError('Elige al menos un amigo.')
        for u in to_users:
            if Membership.objects.is_member(u, self.group):
                raise forms.ValidationError("%(username)s ya es miembro de %(group)s." % {'username': u.first_name,
                                                                      'group': self.group.name})
            if MembershipInvitation.objects.get_invitations_to_user_group(u, self.group):
                raise forms.ValidationError(
                        "Ya has enviado una peticion a %(username)s para unirse a %(group)s." % {
                                                                                    'username': u.first_name,
                                                                                    'group': self.group.name
                                                                                    })
            if MembershipInvitation.objects.get_invitations_from_user_group(u, self.group):
                raise forms.ValidationError(
                        "%(username)s te ha enviado antes una peticion para unirte a %(group)s." % {
                                                                                                        'username': u.first_name,
                                                                                                        'group': self.group.name
                                                                                                        })
        return to_users

    def save(self):
        to_users = self.cleaned_data["to_users"]
        invitations = []
        for u in to_users:
            invitation = MembershipInvitation(group=self.group,
                                              from_user=self.user,
                                              to_user=u)
            invitation.save()
            invitations.append(invitation)
        return invitations


class CommentForm(forms.Form):
    ctext = forms.CharField(label="Introduce tu comentario", max_length=250, widget=forms.Textarea)
    
    def clean_ctext(self):
        ctext = self.cleaned_data['ctext']
        if len(ctext) == 0:
            raise ValidationError('No puedes publicar un comentario vacio')
        return ctext
        
    def save(self, user):
        comment = Comment(author=user, ctext=self.cleaned_data['ctext'])
        comment.save()
        return comment
    
#####

class UserPhotoForm(forms.Form):        
    title = forms.CharField(label="Introduce un titulo para tu foto (Opcional)", max_length=100, required=False)
    image = forms.ImageField(label="Selecciona una foto de tu maquina local")
    
    def clean_image(self):
        MAX_SIZE = 10485760 # 10 MB
        content = self.cleaned_data['image']
        if content._size > MAX_SIZE:
            raise ValidationError('Esta foto es demasiado pesada. Sube otra foto (Peso Maximo: 10 MB)')
        valid_format = content.name.endswith('.jpg') or content.name.endswith('.png') or content.name.endswith('.JPG') or content.name.endswith('.PNG')       
        if not valid_format:
            raise ValidationError('La foto debe tener una extension JPG o PNG. Sube otra foto (Peso Maximo: 10 MB)')
        return content  
        
    def save(self, user):
        photobook, c = UserPhotoBook.objects.get_or_create(user=user)
        photo = UserPhoto(image=self.cleaned_data['image'], 
                            photo_book=photobook,
                            title=self.cleaned_data['title'],
                            creator=user)
        photo.save()
        return photo
        
class GroupPhotoForm(forms.Form):        
    title = forms.CharField(label="Introduce un titulo para tu foto (Opcional)", max_length=100, required=False)
    image = forms.ImageField(label="Selecciona una foto de tu maquina local (Formato JPG o PNG)")
    
    def clean_image(self):
        MAX_SIZE = 10485760 # 10 MB
        content = self.cleaned_data['image']
        if content._size > MAX_SIZE:
            raise ValidationError('Esta foto es demasiado pesada. Sube otra foto (Peso Maximo: 10 MB)')
        valid_format = content.name.endswith('.jpg') or content.name.endswith('.png') or content.name.endswith('.JPG') or content.name.endswith('.PNG')            
        if not valid_format:
            raise ValidationError('La foto debe tener una extension JPG o PNG. Sube otra foto (Peso Maximo: 10 MB)')
        return content   
        
    def save(self, user, group):
        photobook, px = GroupPhotoBook.objects.get_or_create(group=group)
        photo = GroupPhoto(image=self.cleaned_data['image'],
                          photo_book=photobook,
                          title=self.cleaned_data['title'],
                          creator=user)
        photo.save()
        return photo
 
###
 
class TitleForm(forms.Form):
    title = forms.CharField(label="Introduce un nuevo titulo", max_length=100)
    
    def clean(self):
        title = self.cleaned_data["title"]
        if not len(title) > 0:
            raise ValidationError('Titulo invalido')
        return self.cleaned_data

class ShareFriendForm(forms.Form):
    
    def __init__(self, user, queryset, *args, **kwargs):
        super(ShareFriendForm, self).__init__(*args, **kwargs)
        self.fields['to_users'] = forms.ModelMultipleChoiceField(queryset=queryset, 
                                                                    widget=forms.CheckboxSelectMultiple(),
                                                                    label='Selecciona uno o varios amigos')
            
    def clean_to_users(self):
        to_users = self.cleaned_data['to_users']
        if len(to_users) == 0:
            raise ValidationError('Elige al menos un amigo.')
        return to_users
       
#####

class UploadFileForm(forms.Form):

    def __init__(self, user, *args, **kwargs):
        super(UploadFileForm, self).__init__(*args, **kwargs)
        self.fields['file'] = forms.FileField(label='Selecciona el archivo que quieres subir')
        self.user = user
        self.store = UserStore.objects.get(user=user)
    
    def clean_file(self):
        total_size = self.store.get_size()
    
        MAX_SIZE = 31457280 #30 MB
        content = self.cleaned_data['file']
        if content._size > MAX_SIZE:
            raise ValidationError('Este fichero es demasiado pesado. Sube otro fichero (Peso Maximo: 30 MB)')
        forbidden_format = content.name.endswith('.jpg') or content.name.endswith('.png') or content.name.endswith('.JPG') or content.name.endswith('.PNG')      
        if forbidden_format:
            raise ValidationError('El fichero debe tener un formato distinto al de las fotos (JPG o PNG) (Peso Maximo: 30 MB)')
        STORE_MAX = 536870912000 #500 GB
        if total_size+content._size > STORE_MAX:
            raise ValidationError('Se ha excedido el tamano maximo de almacenamiento (500 GB). Por favor, elimina algunos archivos y vuelve a intentarlo')
        return content
        
    def save(self, folder):
        if folder:
            userfile = UserFile(store=self.store,
                                    file=self.cleaned_data['file'],
                                    creator=self.user,
                                    parent_folder=folder)
        else:
            userfile = UserFile(store=self.store,
                                    file=self.cleaned_data['file'],
                                    creator=self.user) 
        userfile.save()
        return userfile
        
class CreateFolderForm(forms.Form):
    
    def __init__(self, user, *args, **kwargs):
        super(CreateFolderForm, self).__init__(*args, **kwargs)
        self.user = user
        self.store = UserStore.objects.get(user=user)
        self.fields['name'] = forms.CharField(label='Introduce el nombre de tu carpeta', max_length=100)
        
    def clean_name(self):
        name = self.cleaned_data['name']
        if not len(name) > 0:
            raise forms.ValidationError('Nombre invalido')
        return name
        
    def save(self, folder):
        if folder:
            userfolder = UserFolder(store=self.store,
                                parent_folder=folder,
                                name=self.cleaned_data['name'],
                                creator=self.user)
        else:
            userfolder = UserFolder(store=self.store,
                                name=self.cleaned_data['name'],
                                creator=self.user)
        userfolder.save()
        return userfolder
        
class SelectFolderForm(forms.Form):
    
    def __init__(self, queryset, *args, **kwargs):
        super(SelectFolderForm, self).__init__(*args, **kwargs)
        self.fields['to_folder'] = forms.ModelChoiceField(queryset=queryset, label='Selecciona una carpeta')
                
    def clean_to_folder(self):
        to_folder = self.cleaned_data['to_folder']
        if not to_folder:
            raise ValidationError('Elige al menos una carpeta.')
        return to_folder
      
