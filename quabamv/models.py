# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
from datetime import datetime
from django.utils.translation import ugettext_lazy as _

from django.db import models
from django.db.models import signals

from django.core.validators import URLValidator
from Quaba import settings
import os

from django.core.mail import EmailMessage

from managers import *

class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name

class UserProfile(models.Model):

    GENDER_CHOICES = (('M', 'Hombre'), ('F', 'Mujer'),)

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=75)
    email = models.EmailField(max_length=50, primary_key=True)
    password = models.CharField(max_length=16)
    age = models.IntegerField()
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    dni = models.CharField(max_length=9)
    
    objects = UserProfileManager()
    
    class Meta:
        ordering = ('last_name','first_name',)
        verbose_name = 'Perfil de Usuario'
        verbose_name_plural = 'Perfiles de Usuario'
        
    def __unicode__(self):
        return "%(name)s %(surname)s" % {'name': self.first_name, 'surname': self.last_name}
        
    def gender_display(self):
        return self.get_gender_display()
                     
    def send_notification(self, message, type=0, id=None):
        if type == 0:
            notification = Notification(message=message, to_user=self)
        else:
            notification = Notification(message=message, to_user=self, type=type, parameter_id=id)
        notification.save() 
        
    def report(self):
        subject = 'Usuario Reportado'  
        message = "Datos del usuario reportado: %(data)s" % {'data': self.__unicode__()}
        msg = EmailMessage(subject, message, to=[settings.EMAIL_HOST_USER])
        msg.send()
    
class Message(models.Model):
    body = models.CharField(max_length=500)
    send_from = models.ForeignKey(UserProfile, related_name='sent_messages')
    send_to = models.ForeignKey(UserProfile, related_name='received_messages', null=True, blank=True)
    parent_msg = models.ForeignKey('self', related_name='next_messages', null=True, blank=True)
    created = models.DateTimeField(default=datetime.now)
    read = models.BooleanField(default=False)
    replied = models.BooleanField(default=False)
        
    objects = MessageManager()
    
    class Meta:
        ordering = ('-created',)
        verbose_name = 'Mensaje'
        verbose_name_plural = 'Mensajes'
        
    def __unicode__(self):
        return 'MSG_FROM_%(em1)s_TO_%(em2)s_M_%(id)s' % {'em1': self.send_from.email, 
                                                            'em2': self.send_to.email,
                                                            'id': self.id}
                                                            
    def report(self):
        subject = 'Mensaje Reportado'  
        message = "Datos del mensaje reportado: %(data)s" % {'data': self.__unicode__()}
        msg = EmailMessage(subject, message, to=[settings.EMAIL_HOST_USER])
        msg.send()
            
####

class Friendship(models.Model):
    from_user = models.ForeignKey(UserProfile)
    to_user = models.ForeignKey(UserProfile, related_name='friends')
    created = models.DateTimeField(default=datetime.now)

    objects = FriendshipManager()
    
    class Meta:
        unique_together = [("to_user", "from_user")]
        ordering = ('-created',)
        
        
class Blocking(models.Model):
    from_user = models.ForeignKey(UserProfile, related_name="blocking")
    to_user = models.ForeignKey(UserProfile, related_name="blocked_by")
    created = models.DateTimeField(default=datetime.now)

    objects = BlockingManager()
    
    class Meta:
        ordering = ('-created',)

class FriendshipInvitation(models.Model):
    from_user = models.ForeignKey(UserProfile, related_name="invitations_from")
    to_user = models.ForeignKey(UserProfile, related_name="invitations_to")
    created = models.DateTimeField(default=datetime.now)
    
    objects = FriendshipInvitationManager() 
    
    class Meta:
        ordering = ('-created',)
    
    def accept(self):
        if not Friendship.objects.are_friends(self.to_user, self.from_user):
            friendship = Friendship(to_user=self.to_user, from_user=self.from_user)
            friendship.save()           
        self.delete()

    def decline(self):
        self.delete()
        blocking = Blocking(from_user=self.to_user,to_user=self.from_user)
        blocking.save()

####

class Comment(models.Model):
    author = models.ForeignKey(UserProfile, related_name='comment_author')
    ctext = models.CharField(max_length=250, blank=True)
         
####

class FriendsGroup(models.Model):
    name = models.CharField(max_length=50) 
    creator = models.ForeignKey(UserProfile, related_name="group_creator")
    created = models.DateTimeField(default=datetime.now)
    
    objects = FriendsGroupManager()
    
    def __unicode__(self):
        return "%(name)s" % {'name': self.name}
    
    class Meta:
        ordering = ('-created',)
        verbose_name = 'Grupo'
        verbose_name_plural = 'Grupos'
    
    def send_notification(self, message, type, id=None):
        members = Membership.objects.members_for_group(self)
        for member in members:
            notification = GroupNotification(message=message, group=self, to_user=member, type=type, parameter_id=id)
            notification.save() 
            
    def send_request_notification(self, message, id):
        notification = GroupNotification(message=message, group=self, to_user=self.creator, type=settings.VIEW_GROUP_REQUEST, parameter_id=id)
        notification.save()
        
    def send_invitation_notification(self, message, email, id):
        member = UserProfile.objects.get_profile_user(email)
        notification = GroupNotification(message=message, group=self, to_user=member, type=settings.VIEW_GROUP_INVITATION, parameter_id=id)
        notification.save()
            
    def report(self):
        subject = 'Grupo Reportado'  
        message = "Datos del grupo reportado: %(data)s" % {'data': self.__unicode__()}
        msg = EmailMessage(subject, message, to=[settings.EMAIL_HOST_USER])
        msg.send()

class GroupComment(models.Model):
    group = models.ForeignKey(FriendsGroup)
    comment = models.ForeignKey(Comment, related_name="group_comment")
    created = models.DateTimeField(default=datetime.now)
    likes = models.IntegerField(default=0)
    
    class Meta:
        ordering = ('-created',)
        
class GroupCommentVote(models.Model):
    user = models.ForeignKey(UserProfile)
    comment = models.ForeignKey(GroupComment)
    voted = models.BooleanField(default=False)
    
    def set_like(self):
        if Membership.objects.is_member(self.user, self.comment.group) and not self.voted:
            self.comment.likes = self.comment.likes+1
            self.comment.save()
            self.voted = True
            self.save()
    
class Membership(models.Model):
    group = models.ForeignKey(FriendsGroup, related_name="group_membership")
    member = models.ForeignKey(UserProfile, related_name="member_of")
    created = models.DateTimeField(default=datetime.now)

    objects = MembershipManager()
    
    class Meta:
        ordering = ('-created',)
    
class MembershipRequest(models.Model):
    group = models.ForeignKey(FriendsGroup, related_name="group_request")
    from_user = models.ForeignKey(UserProfile, related_name="request_user")
    created = models.DateTimeField(default=datetime.now)
    
    objects = MembershipRequestManager()
    
    class Meta:
        ordering = ('-created',)

    def accept(self):
        is_member = Membership.objects.is_member(self.from_user, self.group)
        if not is_member:
            membership = Membership(group=self.group, member=self.from_user)
            membership.save()
        self.delete()
        
    def decline(self):
        self.delete()
  
class MembershipInvitation(models.Model):
    group = models.ForeignKey(FriendsGroup, related_name="group_invitation")
    from_user = models.ForeignKey(UserProfile, related_name="group_invite_from")
    to_user = models.ForeignKey(UserProfile, related_name="group_invite_to")
    created = models.DateTimeField(default=datetime.now)

    objects = MembershipInvitationManager()
    
    class Meta:
        ordering = ('-created',) 
    
    def accept(self):
        if not Membership.objects.is_member(self.to_user, self.group):
            membership = Membership(group=self.group, member=self.to_user)
            membership.save()
        self.delete()

    def decline(self):
        self.delete()
    
####
####
  
class UserPhotoBook(models.Model):
    user = models.OneToOneField(UserProfile, related_name='user_photobook')
    
class UserPhoto(models.Model): 
    image = models.ImageField(upload_to='user/photos/%Y/%m/%d')
    
    photo_book = models.ForeignKey(UserPhotoBook, related_name='user_photo_book')
    creator = models.ForeignKey(UserProfile)
    title = models.CharField(max_length=100, blank=True)
    created = models.DateTimeField(default=datetime.now)
    likes = models.IntegerField(default=0)
    
    objects = UserPhotoManager()
    
    class Meta:
        ordering = ('-created',)
        verbose_name = 'Foto de Usuario'
        verbose_name_plural = 'Fotos de Usuario'
        
    def __unicode__(self):
        return 'UP_U_%(email)s_P_%(pd)s' % {'email': self.creator.email, 'pd': self.id}
    
    def share(self, user):
        photobook = UserPhotoBook.objects.get(user=user)
        shp = UserPhoto(image=self.image, photo_book=photobook, creator=self.creator, title=self.title)
        shp.save()
        return shp
        
    def report(self):
        subject = 'Foto de Usuario Reportada'  
        message = "Datos de la foto de usuario reportada: %(data)s" % {'data': self.__unicode__()}
        msg = EmailMessage(subject, message, to=[settings.EMAIL_HOST_USER])
        msg.send()
        
class UserPhotoVote(models.Model):
    user = models.ForeignKey(UserProfile)
    photo = models.ForeignKey(UserPhoto)
    voted = models.BooleanField(default=False)
    
    def set_like(self):
        if not self.voted:
            self.photo.likes = self.photo.likes+1
            self.photo.save()
            self.voted = True
            self.save()
        
class UserPhotoComment(models.Model):
    photo = models.ForeignKey(UserPhoto)
    comment = models.OneToOneField(Comment, related_name='user_photo_comment')
    created = models.DateTimeField(default=datetime.now)
    likes = models.IntegerField(default=0)
    
    class Meta:
        ordering = ('-created',)
        verbose_name = 'Comentario de Foto de Usuario'
        verbose_name_plural = 'Comentarios de Foto de Usuario'
        
    def __unicode__(self):
        return 'UPC_P_%(pd)s_C_%(cd)s' % {'pd': self.photo.id, 'cd': self.id}
        
    def report(self):
        subject = 'Comentario de Foto de Usuario Reportado'  
        message = "Datos del comentario de foto de usuario reportado: %(data)s" % {'data': self.__unicode__()}
        msg = EmailMessage(subject, message, to=[settings.EMAIL_HOST_USER])
        msg.send()
        
class UserPhotoCommentVote(models.Model):
    user = models.ForeignKey(UserProfile)
    comment = models.ForeignKey(UserPhotoComment)
    voted = models.BooleanField(default=False)
    
    def set_like(self):
        if not self.voted:
            self.comment.likes = self.comment.likes+1
            self.comment.save()
            self.voted = True
            self.save()
      
class GroupPhotoBook(models.Model):
    group = models.OneToOneField(FriendsGroup, related_name='group_photobook')
    
class GroupPhoto(models.Model): 
    image = models.FileField(upload_to='group/photos/%Y/%m/%d')
    
    photo_book = models.ForeignKey(GroupPhotoBook, related_name='group_photo_book')
    likes = models.IntegerField(default=0)
    creator = models.ForeignKey(UserProfile)
    title = models.CharField(max_length=100, blank=True)
    created = models.DateTimeField(default=datetime.now)
    
    objects = GroupPhotoManager()
    
    class Meta:
        ordering = ('-created',)
        
class GroupPhotoVote(models.Model):
    user = models.ForeignKey(UserProfile)
    photo = models.ForeignKey(GroupPhoto)
    voted = models.BooleanField(default=False)
    
    def set_like(self):
        if Membership.objects.is_member(self.user, self.photo.photo_book.group) and not self.voted:
            self.photo.likes = self.photo.likes+1
            self.photo.save()
            self.voted = True
            self.save()
            
class GroupMainPhoto(models.Model):
    photo = models.ForeignKey(GroupPhoto, blank=True, null=True)
    group = models.ForeignKey(FriendsGroup)
    
class GroupPhotoComment(models.Model):
    photo = models.ForeignKey(GroupPhoto)
    comment = models.OneToOneField(Comment, related_name='group_photo_comment')
    created = models.DateTimeField(default=datetime.now)
    likes = models.IntegerField(default=0)
    
    class Meta:
        ordering = ('-created',)
        
class GroupPhotoCommentVote(models.Model):
    user = models.ForeignKey(UserProfile)
    comment = models.ForeignKey(GroupPhotoComment)
    voted = models.BooleanField(default=False)
    
    def set_like(self):
        if Membership.objects.is_member(self.user, self.comment.group) and not self.voted:
            self.comment.likes = self.comment.likes+1
            self.comment.save()
            self.voted = True
            self.save()
             
###

class UserAllComments(models.Model):
    user = models.ForeignKey(UserProfile)
    nc = models.IntegerField(default=0)
    
    def inc(self):
        self.nc = self.nc+1
        self.save()
        
###
 
class UserStore(models.Model):
    user = models.OneToOneField(UserProfile, related_name='user_store')
    
    def get_size(self):
        total_size = 0
        all_files = UserFile.objects.all_files(self)
        if all_files:
            for fi in all_files:
                total_size = total_size+fi.file.size
        all_folders = UserFolder.objects.all_folders(self)
        if all_folders:
            for fo in all_folders:
                total_size = total_size + fo.get_folder_size()
        return total_size
           
class UserFolder(models.Model):  
    store = models.ForeignKey(UserStore, related_name='folder_store')
    name = models.CharField(max_length=100)
    creator = models.ForeignKey(UserProfile, related_name='folder_creator')
    parent_folder = models.ForeignKey('self', null=True, blank=True, related_name='folder_parent') #carpeta contenedora
    created = models.DateTimeField(default=datetime.now)
    
    objects = UserFolderManager()
    
    class Meta:
        ordering = ('-created',)
        verbose_name = 'Carpeta'
        verbose_name_plural = 'Carpetas'
        
    def __unicode__(self):
        return 'FLD_U_%(email)s_F_%(name)s' % {'email': self.creator.email, 'name': self.name}
        
    def report(self):
        subject = 'Carpeta Reportada'  
        message = "Datos de la carpeta reportada: %(data)s" % {'data': self.__unicode__()}
        msg = EmailMessage(subject, message, to=[settings.EMAIL_HOST_USER])
        msg.send()
        
    def get_folder_size(self):
        total_size = 0
        all_files = UserFile.objects.all_content_files(self.store, self)
        if all_files:
            for fi in all_files:
                total_size = total_size+fi.file.size
        all_folders = UserFolder.objects.all_content_folders(self.store, self)
        if all_folders:
            for fo in all_folders:
                total_size = total_size + fo.get_folder_size()
        return total_size
        
    def get_namelist(self):
        namelist = []
        all_content_files = UserFile.objects.all_content_files(self.store, self)
        if all_content_files:
            for cfi in all_content_files:
                namelist.append(cfi.name)   
        all_content_folders = UserFolder.objects.all_content_folders(self.store, self)
        if all_content_folders:
            for cfo in all_content_folders:
                namelist = namelist + cfo.get_namelist()
        return namelist
        
    def share(self, user): #si se supera el limite de almacenamiento del otro usuario, la carpeta se compartira parcialmente
        store, sx = UserStore.objects.get_or_create(user=user)
        shfl = UserFolder(store=store,
                          name=self.name,
                          creator=self.creator)
        shfl.save()
        all_content_folders = UserFolder.objects.all_content_folders(store, self)
        if all_content_folders:
            for acf in all_content_folders:
                if store.get_size()+self.folder_size() <= 536870912000: #500GB
                    shfo = UserFolder(store=store, name=acf.name, 
                                    creator=acf.creator, parent_folder=shfl)
                    shfo.save()
        all_content_files = UserFile.objects.all_content_files(store, self)
        if all_content_folders:
            for acf in all_content_folders:
                if store.get_size()+self.file.size <= 536870912000: #500GB
                    shfi = UserFolder(store=store, parent_folder=shfl, 
                                    file=acf.file, creator=acf.creator)
                    shfi.save()
        
class UserFile(models.Model):   
    store = models.ForeignKey(UserStore, related_name='file_store')
    parent_folder = models.ForeignKey(UserFolder, null=True, blank=True, related_name='file_parent')
    
    file = models.FileField(upload_to='store/%Y/%m/%d', storage=OverwriteStorage())

    creator = models.ForeignKey(UserProfile, related_name='file_creator')
    created = models.DateTimeField(default=datetime.now)

    objects = UserFileManager()
    
    class Meta:
        ordering = ('-created',)
        verbose_name = 'Archivo'
        verbose_name_plural = 'Archivos'
        
    def report(self):
        subject = 'Archivo reportado'  
        message = "Datos del archivo reportado: %(data)s" % {'data': self.__unicode__()}
        msg = EmailMessage(subject, message, to=[settings.EMAIL_HOST_USER])
        msg.send()
                    
    def filename_as_list(self):
        return self.file.name.split('/')
           
    def filesize_in_gb(self):
        MBU = 1024*1024
        GBU = MBU*1024
        if self.file.size < GBU:
            return self.filesize_in_mb()
        else:
            return self.file.size/GBU
        
    def filesize_in_mb(self):
        MBU = 1024*1024
        GBU = MBU*1024
        if self.file.size < MBU:
            return self.filesize_in_kb()
        elif self.file.size >= GBU:
            return self.filesize_in_gb()
        else:
            return self.file.size/MBU
            
    def filesize_in_kb(self):
        MBU = 1024*1024
        if self.file.size >= MBU:
            return self.filesize_in_mb()
        else:
            return self.file.size
           
    def share(self, user): #si se supera el limite de almacenamiento de otro usuario, no se compartira el archivo
        store, sx = UserStore.objects.get_or_create(user=user)
        if store.get_size()+self.file.size <= 536870912000: #500GB
            shf = UserFile(store=store,
                            file=self.file,
                            creator=self.creator)
            shf.save()

  
class UserAdditionalInfo(models.Model):
    user = models.OneToOneField(UserProfile, related_name='userinfo')
    country = models.CharField(max_length=50, blank=True, default='España')
    town = models.CharField(max_length=75, null=True) 
    network = models.CharField(max_length=50, null=True)
    phone_visible = models.BooleanField(default=False)
    phone = models.IntegerField(max_length=12, null=True)
    profile_photo = models.ForeignKey(UserPhoto, related_name='profile_photo', null=True)
  
'''

NOTIFICATIONS

'''   

class Notification(models.Model):
    message = models.TextField()
    to_user = models.ForeignKey(UserProfile, related_name="notify_to")
    type = models.IntegerField(default=0)
    parameter_id = models.CharField(max_length=50, null=True) 
    created = models.DateTimeField(default=datetime.now)
    
    objects = NotificationManager()
                
    class Meta:
        ordering = ('-created',)
        
class GroupNotification(models.Model):
    message = models.TextField()
    group = models.ForeignKey(FriendsGroup)
    to_user = models.ForeignKey(UserProfile, related_name="group_notify_to")
    type = models.IntegerField(default=0)
    parameter_id = models.CharField(max_length=50, null=True) 
    created = models.DateTimeField(default=datetime.now)
    
    objects = GroupNotificationManager()
                
    class Meta:
        ordering = ('-created',)
        
class Misc(models.Model):
    objects = MiscManager()