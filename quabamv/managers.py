from django.db import models
from django.db.models import Q
from django.http import Http404
from random import choice
from string import ascii_lowercase, digits
from datetime import datetime

from models import *

class UserProfileManager(models.Manager):

    def get_profile_user(self, email):
        user = self.get(email=email)
        if not user:
            raise Http404
        else:
            return user
            
    def generate_random_username(self, dni, split=4):
        username = 'Q'.join([choice(ascii_lowercase+digits) for i in xrange(19)])
        username = username+dni
        if split:
            username = '_'.join([username[s:s+split] for s in range(0, len(username), split)])
        return username
                        
class FriendsGroupManager(models.Manager):

    def get_group(self, group_id):
        group = self.get(id=group_id)
        if not group:
            raise Http404
        else:
            return group
            
class FriendshipManager(models.Manager):

    def friends_for_user(self, user):
        friends = []
        qs = self.filter(Q(from_user=user) | Q(to_user=user))
        for friendship in qs:
            if not friendship.to_user in friends and not friendship.from_user in friends:
                if friendship.from_user == user:
                    friends.append(friendship.to_user)
                else:
                    friends.append(friendship.from_user)
        return friends

    def are_friends(self, user1, user2):
        friendship = self.filter(
            Q(from_user=user1, to_user=user2) |
            Q(from_user=user2, to_user=user1)
        )
        if friendship:
            return True
        else:
            return False

    def remove(self, user1, user2):
        friendships = self.filter(from_user=user1, to_user=user2)
        if not friendships:
            friendships = self.filter(from_user=user2, to_user=user1)
        if friendships:
            friendships.delete()
                        
class FriendshipInvitationManager(models.Manager):

    def get_invitation(self,user,id):
        try:
            invitation = self.get(Q(to_user=user) | Q(from_user=user),
                                                     pk=id)
            return invitation
        except self.model.DoesNotExist:
            raise Http404
            
    def get_sent_invitation(self,user,id):
        try:
            invitation = self.get(from_user=user, pk=id)
            return invitation
        except self.model.DoesNotExist:
            raise Http404

    def get_received_invitation(self,user,id):
        try:
            invitation = self.get(to_user=user, pk=id)
            return invitation
        except self.model.DoesNotExist:
            raise Http404

    def remove(self, user1, user2):
        invitations = self.filter(from_user=user1, to_user=user2)
        if not invitations:
            invitations = self.filter(from_user=user2, to_user=user1)
        if invitations:
            invitations.delete()

class BlockingManager(models.Manager):

    def blocked_for_user(self, user):
        blocked = []
        qs = self.filter(from_user=user)
        for blocking in qs:
            if not blocking.to_user in blocked:
                blocked.append(blocking.to_user)
        return blocked
        
    def blocked_to_user(self, user):
        blocked = []
        qs = self.filter(to_user=user)
        for blocking in qs:
            if not blocking.from_user in blocking:
                blocked.append(blocking.from_user)
        return blocked
        
    def get_blocked_user(self, user1, user2):
        try:
            blocking = self.filter(from_user=user1, to_user=user2)
            return blocking
        except self.model.DoesNotExist:
            raise Http404
                
class MembershipManager(models.Manager):
    
    def groups_for_user(self, user):
        groups = []
        qs = self.filter(member=user)
        for user_group in qs:
            if not user_group.group in groups:
                groups.append(user_group.group)
        return groups
           
    def members_for_group(self, group):
        members = []
        qs = self.filter(group=group)
        for membership in qs:
            if not membership.member in members:
                members.append(membership.member)
        return members
        
    def is_member(self, user, group):
        return self.filter(group=group, member=user).count() > 0
        
    def remove(self, user, group):
        memberships = self.filter(group=group, member=user)
        if memberships:
            memberships.delete()       
        
class MembershipInvitationManager(models.Manager):

    def get_invitations_from_user(self, user):
        invitations = self.filter(from_user=user)
        if not invitations:
            return None
        else:
            return invitations

    def get_invitations_to_user(self, user):
        invitations = self.filter(to_user=user)
        if not invitations:
            return None
        else:
            return invitations
            
    def get_invitations_from_user_group(self, user, group):
        invitations = self.filter(from_user=user, group=group)
        if not invitations:
            return None
        else:
            return invitations

    def get_invitations_to_user_group(self, user, group):
        invitations = self.filter(to_user=user, group=group)
        if not invitations:
            return None
        else:
            return invitations

    def get_invitation(self, user, group, id):
        invitation = self.get(Q(to_user=user)|Q(from_user=user),
                                     group=group,
                                     pk=id)
        if not invitation:
            raise Http404
        else:
            return invitation
            
class MembershipRequestManager(models.Manager):

    def get_requests_from_member(self, user):
        reqs = self.filter(from_user=user)
        if not reqs:
            return None
        else:
            return reqs 

    def get_requests_to_group(self, group):
        reqs = self.filter(group=group)
        if not reqs:
            return None
        else:
            return reqs            

    def get_request_member_group(self, group, user):
        req = self.get(group=group, from_user=user)
        if not req:
            raise Http404
        else:
            return req
                                
class MessageManager(models.Manager):

    def inbox_for(self, user):
        return self.filter(send_to=user)

    def outbox_for(self, user):
        return self.filter(send_from=user)
        
    def all_messages(self, user1, user2):
        messages = self.filter(Q(send_from=user1, send_to=user2)|Q(send_from=user2, send_to=user1))
        if not messages:
            return None
        else:
            return messages
            
    def get_message(self, id):
        message = self.get(id=id)
        if not message:
            raise Http404
        else:
            return message  
         
class UserPhotoManager(models.Manager):

    def photos_for_user(self, photobook):
        photos = self.filter(photo_book=photobook)
        if not photos:
            return None
        else:
            return photos
            
    def photos_by_date(self, photobook, year, month):
        photos = self.filter(photo_book=photobook, created__year=year, created__month=month)
        if not photos:
            return None
        else:
            return photos  
        
    def get_photo(self, photobook, photo_id):
        photo = self.get(photo_book=photobook, id=photo_id)
        if not photo:
            raise Http404
        else:
            return photo
            
    def get_shared_photo(self, user, friend_photobook, photo_id):
        photo = self.filter(creator=user, photo_book=friend_photobook, id=photo_id)
        if photo:
            if photo[0].creator.email == user.email:
                return photo
            else:
                return None
        else:
            return None

class GroupPhotoManager(models.Manager):

    def photos_for_group(self, group_photobook):
        photos = self.filter(photo_book=group_photobook)
        if not photos:
            return None
        else:
            return photos
            
    def photos_by_date(self, group_photobook, year, month):
        photos = self.filter(photo_book=group_photobook, created__year=year, created__month=month)
        if not photos:
            return None
        else:
            return photos      
      
    def group_photos_from_user(self, group_photobook, user):
        photo_info = Photo.objects.filter(creator=user)
        photos = self.filter(photo_book=group_photobook, photo_info=photo_info)
        if not photos:
            return None
        else:
            return photos
        
    def get_photo(self, group_photobook, photo_id):
        photo = self.get(photo_book=group_photobook, id=photo_id)
        if not photo:
            raise Http404
        else:
            return photo
             
class NotificationManager(models.Manager):

    def all_notifications(self, user):
        notifications = self.filter(to_user=user)
        if not notifications:
            return None
        else:
            return notifications
            
class GroupNotificationManager(models.Manager):

    def all_notifications(self, group, to_user):
        notifications = self.filter(group=group, to_user=to_user)
        if not notifications:
            return None
        else:
            return notifications
             
class UserFolderManager(models.Manager):

    def all_folders(self, store):
        try: 
            folders = self.filter(store=store)
            return folders
        except self.model.DoesNotExist:
            return None
            
    def all_content_folders(self, store, folder):
        try:
            folders = self.filter(store=store, parent_folder=folder)
            return folders
        except self.model.DoesNotExist:
            return None
                    
    def get_folder(self, store, folder_id):
        try:
            folder = self.get(store=store, id=folder_id)
            return folder
        except self.model.DoesNotExist:
            raise Http404
            
    def get_shared_folder(self, user, friend_store, folder_id):
        try:
            folder = self.filter(store=friend_store, creator=user, id=folder_id)
            return folder
        except self.model.DoesNotExist:
            return None
            
    def search_folders(self, store, term):
        folders = []
        qs = self.filter(store=store)
        for q in qs:
            if term in q.name and not q in folders:
                folders.append(q)
        return folders
        
    def folder_search_folders(self, store, folder, term):
        folders = []
        qs = self.filter(store=store, parent_folder=folder)
        for q in qs:
            if term in q.name and not q in folders:
                folders.append(q)
        return folders 
                                                     
class UserFileManager(models.Manager):

    def all_files(self, store):
        try:
            files = self.filter(store=store)
            return files
        except self.model.DoesNotExist:
            return None
            
    def all_content_files(self, store, folder):
        try:
            files = self.filter(store=store, parent_folder=folder)
            return files
        except self.model.DoesNotExist:
            return None
    
    def get_file(self, store, file_id):
        try:
            file = self.get(store=store, id=file_id)
            return file
        except self.model.DoesNotExist:
            raise Http404
            
    def get_shared_file(self, user, friend_store, file_id):
        try:
            file = self.filter(store=friend_store, creator=user, id=file_id)
            return file
        except self.model.DoesNotExist:
            return None

    def search_files(self, store, term):
        files = []
        qs = self.filter(store=store)
        for q in qs:
            if term in q.file.name and not q in files:
                files.append(q)
        return files
        
    def folder_search_files(self, store, folder, term):
        files = []
        qs = self.filter(store=store, parent_folder=folder)
        for q in qs:
            if term in q.file.name and not q in files:
                files.append(q)
        return files  
        
class MiscManager(models.Manager):
    def date_name(self, year, number_month):
        if number_month == '1':
            return 'Enero del %(year)s' % {'year': year}
        elif number_month == '2':
            return 'Febrero del %(year)s' % {'year': year}
        elif number_month == '3':
            return 'Marzo del %(year)s' % {'year': year}
        elif number_month == '4':
            return 'Abril del %(year)s' % {'year': year}
        elif number_month == '5':
            return 'Mayo del %(year)s' % {'year': year}
        elif number_month == '6':
            return 'Junio del %(year)s' % {'year': year}
        elif number_month == '7':
            return 'Julio del %(year)s' % {'year': year}
        elif number_month == '8':
            return 'Agosto del %(year)s' % {'year': year}
        elif number_month == '9':
            return 'Septiembre del %(year)s' % {'year': year}
        elif number_month == '10':
            return 'Octubre del %(year)s' % {'year': year}
        elif number_month == '11':
            return 'Noviembre del %(year)s' % {'year': year}
        elif number_month == '12':
            return 'Diciembre del %(year)s' % {'year': year}
        else:
            return '<no_valid_date>' 
            