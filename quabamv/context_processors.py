from django.conf import settings

def notification_type(context):
    return {
        'VIEW_USER': settings.VIEW_USER,
        'VIEW_MESSAGES': settings.VIEW_MESSAGES,
        'VIEW_USER_PHOTO': settings.VIEW_USER_PHOTO,
        'VIEW_USER_INVITATION': settings.VIEW_USER_INVITATION,
        'VIEW_USER_FOLDER': settings.VIEW_USER_FOLDER,
        'VIEW_STORE': settings.VIEW_STORE,
        'VIEW_GROUP': settings.VIEW_GROUP,
        'VIEW_GROUP_MEMBERS': settings.VIEW_GROUP_MEMBERS,
        'VIEW_GROUP_REQUEST': settings.VIEW_GROUP_REQUEST,
        'VIEW_GROUP_INVITATION': settings.VIEW_GROUP_INVITATION,
        'VIEW_GROUP_PHOTO': settings.VIEW_GROUP_PHOTO
    }
    