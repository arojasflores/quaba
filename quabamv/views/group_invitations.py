from django.db.models import Q
from django.shortcuts import render, render_to_response
from django.template import RequestContext

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.conf import settings

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

from quabamv.models import *
from quabamv.forms import *

@csrf_protect  
@login_required(login_url='/quaba/login/')
def invite_to_group(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        us = UserProfile.objects.all()
        us = us.exclude(email=user.email)
        for u in us:
            if not Friendship.objects.are_friends(u, user) or Membership.objects.is_member(u, group):
                us = us.exclude(email=u.email)
            minv = MembershipInvitation.objects.filter(group=group, to_user=u)
            if minv:
                us = us.exclude(email=u.email)
        if 'q' in request.GET:
            q = request.GET['q']
            if q:
                for term in q.split():
                    us = us.filter(Q(first_name__icontains=term)|Q(last_name__icontains=term))
        if request.method == 'POST':
            form = InviteGroupForm(user, group, us, request.POST)
            if form.is_valid():
                invitations = form.save()
                friends = form.cleaned_data['to_users']
                l = len(friends)
                if l > 1:
                    msg = 'Has invitado a %(num)d amigos a unirse a %(group)s' % {'num': len(friends), 'group': group.name}
                else:
                    msg = 'Has invitado a un amigo a unirse a %(group)s' % {'group': group.name}
                user.send_notification(msg)
                for friend in friends:   
                    message = "%(name)s %(surname)s te ha invitado a unirte a %(groupname)s" % {'name': user.first_name,
                                                                                                'surname': user.last_name,
                                                                                                'groupname': group.name}  
                    inv = MembershipInvitation.objects.get(from_user=user, to_user=friend, group=group)
                    group.send_invitation_notification(message, friend.email, inv.id)                  
                return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
        else:
            form = InviteGroupForm(user, group, us)
        return render(request, 'quaba/groups/invitations/invite_to_group.html',
                                {'form': form, 'group': group, 'friends': us})
                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_member_received_invitations(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    invitation_list = MembershipInvitation.objects.get_invitations_to_user(user)
    if invitation_list:
        paginator = Paginator(invitation_list, 10)
        page = request.GET.get('page')
        try:
            invitations = paginator.page(page)
        except PageNotAnInteger:
            invitations = paginator.page(1)
        except EmptyPage:
            invitations = paginator.page(paginator.num_pages)
    else:
        invitations = None
    return render_to_response('quaba/groups/invitations/invitations_member_list.html',
                              {'invitations': invitations,
                              'status': 'received'},
                              context_instance=RequestContext(request))
                             
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_member_sent_invitations(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    invitation_list = MembershipInvitation.objects.get_invitations_from_user(user)
    if invitation_list:
        paginator = Paginator(invitation_list, 10)
        page = request.GET.get('page')
        try:
            invitations = paginator.page(page)
        except PageNotAnInteger:
            invitations = paginator.page(1)
        except EmptyPage:
            invitations = paginator.page(paginator.num_pages)
    else:
        invitations = None
    return render_to_response('quaba/groups/invitations/invitations_member_list.html',
                              {'invitations': invitations,
                              'status': 'sent'},
                              context_instance=RequestContext(request))
          
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_group_received_invitations(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    invitation_list = MembershipInvitation.objects.get_invitations_to_user_group(user, group)
    if invitation_list:
        paginator = Paginator(invitation_list, 10)
        page = request.GET.get('page')
        try:
            invitations = paginator.page(page)
        except PageNotAnInteger:
            invitations = paginator.page(1)
        except EmptyPage:
            invitations = paginator.page(paginator.num_pages)
    else:
        invitations = None
    return render_to_response('quaba/groups/invitations/invitations_group_list.html',
                              {'invitations': invitations,
                              'group': group,
                              'status': 'received'},
                              context_instance=RequestContext(request))                                   
                            
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_group_sent_invitations(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    invitation_list = MembershipInvitation.objects.get_invitations_from_user_group(user, group)
    if invitation_list:
        paginator = Paginator(invitation_list, 10)
        page = request.GET.get('page')
        try:
            invitations = paginator.page(page)
        except PageNotAnInteger:
            invitations = paginator.page(1)
        except EmptyPage:
            invitations = paginator.page(paginator.num_pages)
    else:
        invitations = None
    return render_to_response('quaba/groups/invitations/invitations_group_list.html',
                              {'invitations': invitations,
                              'group': group,
                              'status': 'sent'},
                              context_instance=RequestContext(request)) 
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def delete_group_invitation(request, group_id, invitation_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    invitation = MembershipInvitation.objects.get_invitation(user, group, invitation_id)    
    if 'delete' in request.POST:
        invitation.delete()
        return HttpResponseRedirect(reverse('groups_sent_invitations', args=(group.id,)))
    elif 'no_delete' in request.POST:
        return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
    return render(request, 'quaba/groups/invitations/invitation_remove.html')
                               
@csrf_protect  
@login_required(login_url='/quaba/login/')
def respond_to_group_invitation(request, group_id, invitation_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    invitation = MembershipInvitation.objects.get_invitation(user, group, invitation_id)
    
    ns = GroupNotification.objects.all_notifications(group, user)
    if ns:
        for n in ns:
            if n.type == settings.VIEW_GROUP_INVITATION and n.group.id == group.id and n.parameter_id == invitation.id:
                n.delete()
    
    if invitation.to_user.email == user.email:
        if 'accept' in request.POST:
            invitation.accept()  
            group_message = "%(name)s %(surname)s se ha unido a %(groupname)s" % {'name': user.first_name,
                                                                              'surname': user.last_name,
                                                                              'groupname': group.name}                                                                  
            group.send_notification(group_message, settings.VIEW_GROUP_MEMBERS)
            return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
        elif 'decline' in request.POST:
            invitation.decline()        
            return HttpResponseRedirect(reverse('groups_menu'))
    return render(request, 'quaba/groups/invitations/invitation_group_show.html', {'user': user, 'invitation': invitation})