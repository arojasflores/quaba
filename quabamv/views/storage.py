from django.db.models import Q
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required

from django.conf import settings
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

import os, StringIO, zipfile 

from quabamv.models import *
from quabamv.forms import *

@csrf_protect  
@login_required(login_url='/quaba/login/')
def store_view(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, created = UserStore.objects.get_or_create(user=user)
    folders = UserFolder.objects.all_folders(store)
    files = UserFile.objects.all_files(store)
    is_empty = True
    if folders or files:
        is_empty = False
    gbu = 1024*1024*1024
    store_max = 5*gbu
    total_size = store.get_size()
    return render_to_response('quaba/storage/view.html',
                                {'folders': folders, 'files': files, 
                                 'empty': is_empty, 'total_size': total_size,
                                 'gbu': gbu, 'kbu': 1024, 'user': user,
                                 'mbu': gbu/1024, 'perc': 100*total_size/store_max},
                                context_instance=RequestContext(request))
                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def file_report(request, file_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, sx = UserStore.objects.get_or_create(user=user)
    userfile = UserFile.objects.get_file(store, file_id)
    if userfile.creator.email != user.email:
        if 'report' in request.POST:
            userfile.report()
            msg = 'Has reportado un archivo de %(name)s %(surname)s' % {'name': userfile.creator.first_name,
                                                                       'surname': userfile.creator.last_name} 
            user.send_notification(msg)
            return HttpResponseRedirect(reverse('main_menu'))
        elif 'no_report' in request.POST:
            return HttpResponseRedirect(reverse('store_view'))
        return render(request, 'quaba/storage/report.html')
                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def upload_file(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, sx = UserStore.objects.get_or_create(user=user)
    if request.method == 'POST':
        form = UploadFileForm(user, request.POST, request.FILES)
        if form.is_valid(): 
            file = form.save(None)
            
            files = UserFile.objects.all_files(store)
            files = files.exclude(id=file.id)
            folders = UserFolder.objects.all_folders(store)
            for fold in folders:
                files = files.exclude(parent_folder=fold) 
            for fi in files:
                if fi.file.name == file.file.name:
                    fi.delete()
            
            user.send_notification('Archivo subido con exito')
            return HttpResponseRedirect(reverse('store_view'))
    else:
        form = UploadFileForm(user)
    return render_to_response('quaba/storage/upload_file.html',
                              {'form': form},
                              context_instance=RequestContext(request))
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def upload_file_in_folder(request, folder_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, sx = UserStore.objects.get_or_create(user=user)
    parent_folder = UserFolder.objects.get_folder(store, folder_id)
    if request.method == 'POST':
        form = UploadFileForm(user, request.POST, request.FILES)
        if form.is_valid():
            file = form.save(parent_folder)
            
            files = UserFile.objects.all_content_files(store, parent_folder)
            files = files.exclude(id=file.id)
            folders = UserFolder.objects.all_content_folders(store, parent_folder)
            for fold in folders:
                files = files.exclude(parent_folder=fold) 
            for fi in files:
                if fi.file.name == file.file.name:
                    fi.delete()
            
            msg = 'Archivo subido en %(fname)s con exito' % {'fname': parent_folder.name}
            user.send_notification(msg)
            return HttpResponseRedirect(reverse('folder_view', args=(parent_folder.id,)))
    else:
        form = UploadFileForm(user)
    return render_to_response('quaba/storage/folders/upload_file.html',
                              {'form': form, 'folder': parent_folder},
                              context_instance=RequestContext(request)) 
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def search_in_store(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, created = UserStore.objects.get_or_create(user=user)
    empty = False
    if 'q' in request.GET:
        q = request.GET['q']
        folders = UserFolder.objects.all_folders(store)
        files = UserFile.objects.all_files(store)
        if folders or files:
            for f in folders:
                folders = folders.exclude(parent_folder=f)
                files = files.exclude(parent_folder=f)
                if q:
                    for term in q.split():
                        if folders:
                            folders = UserFolder.objects.search_folders(store, term)
                        if files:
                            files = UserFile.objects.search_files(store, term)
                return render(request, 'quaba/search/search_store_results.html', 
                                {'folders': folders, 'files': files, 
                                'query': q, 'empty': empty})
        else: 
            empty = True
    return render(request, 'quaba/search/search_store_form.html')
                                                                                         
@csrf_protect  
@login_required(login_url='/quaba/login/')
def delete_file(request, file_id):    
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, sx = UserStore.objects.get_or_create(user=user)
    userfile = UserFile.objects.get_file(store, file_id)
    in_folder = False
    if userfile.parent_folder:
        in_folder = True
        pfid = userfile.parent_folder.id
    if 'delete' in request.POST:
        if user.email == userfile.creator.email:
            friends = Friendship.objects.friends_for_user(user)
            for friend in friends:
                fstore, fstorereated = UserStore.objects.get_or_create(user=friend)
                f = UserFile.objects.get_shared_file(user, fstore, userfile.id)
                if f is not None:
                    f.delete()
        userfile.delete()
        user.send_notification('Archivo eliminado con exito')
        if in_folder:
            return HttpResponseRedirect(reverse('folder_view', args=(pfid,)))
        else:
            return HttpResponseRedirect(reverse('store_view'))
    elif 'no_delete' in request.POST:    
        if in_folder:
            return HttpResponseRedirect(reverse('folder_view', args=(pfid,)))
        else:
            return HttpResponseRedirect(reverse('store_view'))
    return render(request, 'quaba/storage/delete.html') 

@csrf_protect  
@login_required(login_url='/quaba/login/')
def share_file(request, file_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, sx = UserStore.objects.get_or_create(user=user)
    userfile = UserFile.objects.get_file(store, file_id)
    
    us = UserProfile.objects.all()
    us = us.exclude(email=user.email)
    for u in us:
        if not Friendship.objects.are_friends(u, user):
            us = us.exclude(email=u.email)
    if 'q' in request.GET:
        q = request.GET['q']
        if q:
            for term in q.split():
                us = us.filter(Q(first_name__icontains=term)|Q(last_name__icontains=term))  
                
    if request.method == 'POST':
        form = ShareFriendForm(user, us, request.POST)
        if form.is_valid():
            friends = form.cleaned_data['to_users']
            for friend in friends:
                fstore, fsx = UserStore.objects.get_or_create(user=friend)
                if UserFile.objects.get_shared_file(user, fstore, file_id):
                    user_message = "Este archivo ya ha sido compartido con %(name)s %(surname)s" % {'name': friend.first_name, 
                                                                                                    'surname': friend.last_name}
                else:
                    userfile.share(friend)  
                    user_message = "Archivo compartido con %(name)s %(surname)s con exito" % {'name': friend.first_name, 
                                                                                                'surname': friend.last_name}
                    friend_message = "%(name)s %(surname)s ha compartido un archivo contigo." % {'name': user.first_name, 
                                                                                                'surname': user.last_name}                                    
                    friend.send_notification(friend_message, settings.VIEW_STORE)
                user.send_notification(user_message) 
            return HttpResponseRedirect(reverse('store_view'))  
    else:
        form = ShareFriendForm(user, us)
    return render_to_response('quaba/storage/file_share.html',
                                {'form': form},
                                context_instance=RequestContext(request)) 

@csrf_protect  
@login_required(login_url='/quaba/login/')
def download_file(request, file_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, sx = UserStore.objects.get_or_create(user=user)
    userfile = UserFile.objects.get_file(store, file_id)
    filename = userfile.filename_as_list()[-1]
    response = HttpResponse(userfile.file.read(), content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response
                   
@csrf_protect  
@login_required(login_url='/quaba/login/')
def folder_view(request, folder_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, created = UserStore.objects.get_or_create(user=user)
    folder = UserFolder.objects.get_folder(store, folder_id)
    notifications = Notification.objects.all_notifications(user)
    if notifications:
        for n in notifications:
            if n.type == settings.VIEW_USER_FOLDER and n.parameter_id == folder.id:
                n.delete()
    content_folders = UserFolder.objects.all_content_folders(store, folder)
    content_files = UserFile.objects.all_content_files(store, folder)
    is_empty = True
    if content_folders or content_files:
        is_empty = False
    if folder.creator.email != user.email:
        fcr = False
    else:
        fcr = True
    return render_to_response('quaba/storage/folders/folder_view.html',
                                {'folder': folder, 'cfolders': content_folders, 
                                'cfiles': content_files, 'fcr': fcr, 
                                'empty': is_empty, 'mbu': 1024*1024*1024},
                                context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def folder_report(request, folder_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, created = UserStore.objects.get_or_create(user=user)
    folder = UserFolder.objects.get_folder(store, folder_id)
    if folder.creator.email != user.email:
        if 'report' in request.POST:
            folder.report()
            msg = 'Has reportado una carpeta de %(name)s %(surname)s' % {'name': folder.creator.first_name,
                                                                       'surname': folder.creator.last_name} 
            user.send_notification(msg)
            return HttpResponseRedirect(reverse('main_menu'))
        elif 'no_report' in request.POST:
            return HttpResponseRedirect(reverse('folder_view', args=(folder_id,)))
        return render(request, 'quaba/storage/report.html')
                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def search_in_folder(request, folder_id):  
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, created = UserStore.objects.get_or_create(user=user)
    folder = UserFolder.objects.get_folder(store, folder_id)
    empty = False
    if 'q' in request.GET:
        folders = UserFolder.objects.all_content_folders(store, folder)
        files = UserFile.objects.all_content_files(store, folder)
        if folders or files:
            for f in folders:
                folders = folders.exclude(parent_folder=f)
                files = files.exclude(parent_folder=f)
            q = request.GET['q']
            if q:
                for term in q.split():
                    if folders:
                        folders = UserFolder.objects.folder_search_folders(store, folder, term)
                    if files:
                        files = UserFile.objects.folder_search_files(store, folder, term)
            return render(request, 'quaba/search/search_folder_results.html', 
                                {'folder': folder, 'folders': folders, 
                                'files': files, 'query': q, 'empty': empty})
        else:
            empty = True
    return render(request, 'quaba/search/search_folder_form.html', {'folder': folder})

@csrf_protect  
@login_required(login_url='/quaba/login/')
def create_folder(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    if request.method == 'POST':
        form = CreateFolderForm(user, request.POST)
        if form.is_valid():
            folder = form.save(None) 
            user.send_notification('Carpeta creada con exito')
            return HttpResponseRedirect(reverse('folder_view', args=(folder.id,)))
    else:
        form = CreateFolderForm(user)
    return render_to_response('quaba/storage/create_folder.html',
                                {'form': form},
                                context_instance=RequestContext(request))    
                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def create_folder_in_folder(request, folder_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, created = UserStore.objects.get_or_create(user=user)
    parent_folder = UserFolder.objects.get_folder(store, folder_id)
    if request.method == 'POST':
        form = CreateFolderForm(user, request.POST)
        if form.is_valid():
            folder = form.save(parent_folder)
            msg = 'Subcarpeta de %(fname)s creada con exito' % {'fname': parent_folder.name}
            user.send_notification(msg)
            return HttpResponseRedirect(reverse('folder_view', args=(parent_folder.id,)))
    else:
        form = CreateFolderForm(user)
    return render_to_response('quaba/storage/folders/create_folder.html',
                                {'form': form, 'folder': parent_folder},
                                context_instance=RequestContext(request))  
    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def rename_folder(request, folder_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, created = UserStore.objects.get_or_create(user=user)
    folder = UserFolder.objects.get_folder(store, folder_id)
    if user.email == folder.creator.email:
        if request.method == 'POST':
            form = TitleForm(request.POST)
            if form.is_valid():
                old_name = folder.name
                new_name = form.cleaned_data['title']
                if new_name != old_name:
                    folder.name = new_name
                    folder.save()
                    user.send_notification('Carpeta renombrada con exito')
                    friends = Friendship.objects.friends_for_user(user)
                    if friends:
                        for friend in friends:
                            fstore, fsx = UserStore.objects.get_or_create(user=friend)
                            fld = UserFolder.objects.get_shared_folder(user, fstore, folder.id)
                            if fld:
                                friend_message = "%(name)s %(surname)s ha renombrado una carpeta compartida contigo" % {'name': user.first_name, 
                                                                                                                        'surname': user.last_name}
                                friend.send_notification(friend_message, settings.VIEW_USER_FOLDER, fld.id)  
                    return HttpResponseRedirect(reverse('folder_view', args=(folder.id,)))
                else:
                    return HttpResponseRedirect(reverse('rename_folder', args=(folder.id,)))
        else:
            form = TitleForm(initial={'title': folder.name})
        return render_to_response('quaba/storage/folders/folder_rename.html',
                              {'form': form, 'folder': folder},
                              context_instance=RequestContext(request))
    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def delete_folder(request, folder_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, stx = UserStore.objects.get_or_create(user=user)
    userfolder = UserFolder.objects.get_folder(store, folder_id)
    in_folder = False
    if userfolder.parent_folder:
        in_folder = True
        pfid = userfolder.parent_folder.id
    deleted = False
    if 'delete' in request.POST:
        deleted = True
        if user.email == userfolder.creator.email:
            friends = Friendship.objects.friends_for_user(user)
            for friend in friends:
                fstore, fsx = UserStore.objects.get_or_create(user=friend)
                fld = UserFolder.objects.get_shared_folder(user, fstore, userfolder.id)
                if fld is not None:
                    notifications = Notification.objects.all_notifications(friend)
                    if notifications:
                        for n in notifications:
                            if n.type == settings.VIEW_USER_FOLDER and n.parameter_id == fld.id:
                                n.delete()
                    fld.delete()  
        all_folders = UserFolder.objects.all_content_folders(store, userfolder)
        all_folders.delete()
        all_files = UserFile.objects.all_content_files(store, userfolder)
        all_files.delete()
        
        userfolder.delete()
        user.send_notification('Carpeta eliminada con exito')
        if in_folder:
            return HttpResponseRedirect(reverse('folder_view', args=(pfid,)))
        else:
            return HttpResponseRedirect(reverse('store_view'))
    elif 'no_delete' in request.POST:
        if in_folder:
            return HttpResponseRedirect(reverse('folder_view', args=(userfolder.id,)))
        else:
            return HttpResponseRedirect(reverse('store_view'))
    return render_to_response('quaba/storage/delete.html',
                              {'deleted': deleted, 'in_folder': in_folder, 'parent_folder': userfolder.parent_folder},
                              context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def share_folder(request, folder_id, redirect_to_view=store_view):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, stx = UserStore.objects.get_or_create(user=user)
    userfolder = UserFolder.objects.get_folder(store, folder_id)
    
    us = UserProfile.objects.all()
    us = us.exclude(email=user.email)
    for u in us:
        if not Friendship.objects.are_friends(u, user):
            us = us.exclude(email=u.email)
    if 'q' in request.GET:
        q = request.GET['q']
        if q:
            for term in q.split():
                us = us.filter(Q(first_name__icontains=term)|Q(last_name__icontains=term)) 
    if request.method == 'POST':
        form = ShareFriendForm(user, us, request.POST)
        if form.is_valid():
            friends = form.cleaned_data['to_users']
            for friend in friends:
                fstore, fsx = UserStore.objects.get_or_create(user=friend)
                if UserFolder.objects.get_shared_folder(user, fstore, folder_id):
                    user_message = "Esta carpeta ya ha sido compartida con %(name)s %(surname)s" % {'name': friend.first_name,                                                                                      'surname': friend.last_name}
                else:
                    f = userfolder.share(friend)
                    user_message = "Carpeta compartida con %(name)s %(surname)s con exito" % {'name': friend.first_name, 
                                                                                                'surname': friend.last_name}
                    friend_message = "%(name)s %(surname)s ha compartido una carpeta contigo." % {'name': user.first_name, 
                                                                                    'surname': user.last_name}                                    
                    friend.send_notification(friend_message, settings.VIEW_USER_FOLDER, f.id)
                user.send_notification(user_message) 
            return HttpResponseRedirect(reverse('store_view'))  
    else:
        form = ShareFriendForm(user, us)
    return render_to_response('quaba/storage/folders/folder_share.html',
                                {'form': form, 'folder': userfolder},
                                context_instance=RequestContext(request))                 
  
@csrf_protect  
@login_required(login_url='/quaba/login/')
def download_folder(request, folder_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store, stx = UserStore.objects.get_or_create(user=user)
    folder = UserFolder.objects.get_folder(store, folder_id)
    
    filenames = folder.get_namelist()
    
    zip_name = "quaba%(name)s" % {'name': folder.name}
    zip_fullname = "%(zn)s.zip" % {'zn': zip_name}
    s = StringIO.StringIO()
    zf = zipfile.ZipFile(s, 'w')
    
    for fpath in filenames:
        fdir, fname = os.path.split(fpath)
        zip_path = os.path.join(zip_name, fname)
        zf.write(fpath, zip_path)
    zf.close()
    resp = HttpResponse(s.getvalue(), mimetype="application/x-zip-compressed")
    resp['Content-Disposition'] = 'attachment; filename=%s' % zip_fullname
    return resp
        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def move_into_folder(request, oid, id):
    user = UserProfile.objects.get_profile_user(request.user.email)  
    store = UserStore.objects.get(user=user)    
    
    if oid == 1:
        item = UserFolder.objects.get_folder(store, id)
        foldername = item.name
        user_message = "Carpeta movida con exito"
    elif oid == 2:
        item = UserFile.objects.get_file(store, id)  
        foldername = None
        user_message = "Archivo movido con exito"   
    
    if item.parent_folder:
        folders = UserFolder.objects.filter(store=store, parent_folder=item.parent_folder) 
    else:
        folders = UserFolder.objects.filter(store=store)  
    if foldername:
        folders = folders.exclude(name=foldername)
    
    if request.method == 'POST':
        form = SelectFolderForm(folders, request.POST)
        if form.is_valid():
            new_parent_folder = form.cleaned_data['to_folder']
            
            if item.parent_folder:        
                new_parent_folder.parent_folder = item.parent_folder
            
            item.parent_folder = new_parent_folder
            item.save()
            user_message = "%(msg)s a %(foldername)s" % {'msg': user_message, 
                                                            'foldername': new_parent_folder.name}
            user.send_notification(user_message)
            return HttpResponseRedirect(reverse('folder_view', args=(new_parent_folder.id,)))  
    else:
        form = SelectFolderForm(folders)
    return render_to_response('quaba/storage/move_into.html',
                                 {'form': form, 'item': item},
                                 context_instance=RequestContext(request))
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def move_out_of_folder(request, oid, id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    store = UserStore.objects.get(user=user)    
    
    if oid == 1:
        item = UserFolder.objects.get_folder(store, id)
        user_message = "Carpeta removida con exito"
    elif oid == 2:
        item = UserFile.objects.get_file(store, id)
        user_message = "Archivo removido con exito"
        
    if item.parent_folder:
    
        if 'move_out' in request.POST:
            user_message = "%(msg)s de %(foldername)s" % {'msg': user_message, 
                                                  'foldername': item.parent_folder.name}
            if not item.parent_folder.parent_folder:
                item.parent_folder = None
                item.save()
                user.send_notification(user_message)
                return HttpResponseRedirect(reverse('store_view'))
            else:      
                item.parent_folder = item.parent_folder.parent_folder
                item.save()
                user.send_notification(user_message)
                return HttpResponseRedirect(reverse('folder_view', 
                                            args=(item.parent_folder.parent_folder.id,)))
        elif 'no_move' in request.POST:
            return HttpResponseRedirect(reverse('folder_view', 
                                            args=(item.parent_folder.id,)))
    else:
        return HttpResponseRedirect(reverse('store_view'))
    return render(request, 'quaba/storage/folders/move_out.html', {'folder': item.parent_folder})
                                            
                                    
