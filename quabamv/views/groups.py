from django.db.models import Q
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.conf import settings

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

from quabamv.models import *
from quabamv.forms import *

@csrf_protect  
@login_required(login_url='/quaba/login/')
def groups_menu(request):
    return render(request, 'quaba/groups/menu.html')

@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_groups(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group_list = Membership.objects.groups_for_user(user)
    if group_list:
        paginator = Paginator(group_list, 10)
        page = request.GET.get('page')
        try:
            groups = paginator.page(page)
        except PageNotAnInteger:
            groups = paginator.page(1)
        except EmptyPage:
            groups = paginator.page(paginator.num_pages)
    else:
        groups = None
    cgs = FriendsGroup.objects.filter(creator=user)
    return render_to_response('quaba/groups/groups_list.html',
                              {'groups': groups, 'cgs': cgs},
                              context_instance=RequestContext(request))
         
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_friend_groups(request,email):
    user = UserProfile.objects.get_profile_user(request.user.email)
    friend = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(user, friend):
        group_list = Membership.objects.groups_for_user(friend)
        if group_list:
            paginator = Paginator(group_list, 10)
            page = request.GET.get('page')
            try:
                groups = paginator.page(page)
            except PageNotAnInteger:
                groups = paginator.page(1)
            except EmptyPage:
                groups = paginator.page(paginator.num_pages)
        else:
            groups = None
        user_groups = Membership.objects.groups_for_user(user)
        return render_to_response('quaba/groups/friend_groups_list.html',
                              {'groups': groups, 'friend': friend, 'ugroups': user_groups},
                              context_instance=RequestContext(request))
         
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_view(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    comm_list = GroupComment.objects.filter(group=group)
    
    if comm_list:
        paginator = Paginator(comm_list, 10)
        page = request.GET.get('page')
        try:
            comm = paginator.page(page)
        except PageNotAnInteger:
            comm = paginator.page(1)
        except EmptyPage:
            comm = paginator.page(paginator.num_pages)
    else:
        comm = None
    gcr = False
    mainphoto,x = GroupMainPhoto.objects.get_or_create(group=group)
    
    if Membership.objects.is_member(user, group):
        join = True
        notifications = GroupNotification.objects.all_notifications(group, user)
        if notifications:
            for n in notifications:
                if n.type == settings.VIEW_GROUP and n.group.id == group.id and n.parameter_id == group.id:
                    n.delete()
        if user.email == group.creator.email:
            gcr = True
    else:
        join = False    
    return render_to_response('quaba/groups/group_view.html',
                              {'group': group, 'gcr': gcr, 'comm': comm, 'join': join, 'mainphoto': mainphoto},
                              context_instance=RequestContext(request))
                                                        
@csrf_protect  
@login_required(login_url='/quaba/login/')                        
def group_report(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if not Membership.objects.is_member(user, group):
        if 'report' in request.POST:
            group.report()
            msg = 'Has reportado el grupo %(name)s' % {'name': group.name}
            user.send_notification(msg)
            return HttpResponseRedirect(reverse('main_menu'))
        elif 'no_report' in request.POST:
            return HttpResponseRedirect(reverse('group_view', args=(group_id,)))
        return render(request, 'quaba/groups/report.html', {'group': group})
                                                        
@csrf_protect  
@login_required(login_url='/quaba/login/')                    
def create_group(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    if request.method == 'POST':
        form = FriendsGroupForm(request.POST)
        if form.is_valid():
            group = form.save(user)
            
            #INCLUIR AL CREADOR COMO MIEMBRO DEL GRUPO
            group_adm = Membership(group=group, member=user)
            group_adm.save()
    
            #CREAR LIBRO DE FOTOS DEL GRUPO
            photobook, pbx = GroupPhotoBook.objects.get_or_create(group=group)
            mainphoto, mpx = GroupMainPhoto.objects.get_or_create(group=group)

            return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
    else:
        form = FriendsGroupForm()
    return render_to_response('quaba/groups/group_edit/group_create.html',
                              {'form': form},
                              context_instance=RequestContext(request)) 

@csrf_protect  
@login_required(login_url='/quaba/login/')
def rename_group(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if user.email == group.creator.email:
        if request.method == 'POST':
            form = TitleForm(request.POST)
            if form.is_valid():
                old_name = group.name
                new_name = form.cleaned_data['title']
                if new_name != old_name:
                    group.name = new_name
                    group.save()
                    message = "%(name)s %(surname)s ha cambiado el nombre del grupo %(oldname)s a %(groupname)s" % {'name': user.first_name,
                                                                                                                'surname': user.last_name,
                                                                                                                'oldname': old_name,
                                                                                                                'groupname': group.name}
                    group.send_notification(message, settings.VIEW_GROUP) 
                    return HttpResponseRedirect(reverse('group_view', args=(group.id,))) 
                else:
                    return HttpResponseRedirect(reverse('rename_group', args=(group.id,)))  
        else:
            form = TitleForm(initial={'title': group.name})
        return render_to_response('quaba/groups/group_edit/group_rename.html',
                              {'form': form, 'group': group},
                              context_instance=RequestContext(request))
                                                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_group_members(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        notifications = GroupNotification.objects.all_notifications(group, user)
        if notifications:
            for n in notifications:
                if n.type == settings.VIEW_GROUP_MEMBERS and n.group.id == group.id and n.parameter_id == group.id:
                    n.delete()
        member_list = Membership.objects.members_for_group(group)
        if member_list:
            paginator = Paginator(member_list, 10)
            page = request.GET.get('page')
            try:
                members = paginator.page(page)
            except PageNotAnInteger:
                members = paginator.page(1)
            except EmptyPage:
                members = paginator.page(paginator.num_pages)    
        else:
            members = None
        return render_to_response('quaba/groups/membership/group_members_list.html',
                                    {'group': group, 'user': user, 'members': members},
                                    context_instance=RequestContext(request))
        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_new_comment(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        if request.method == "POST":
            form = CommentForm(request.POST)
            if form.is_valid():
                c = form.save(user)
                comment = GroupComment(group=group,comment=c)
                comment.save()  
                
                numc, numcx = UserAllComments.objects.get_or_create(user=user)
                numc.inc()
                
                message = "%(name)s %(surname)s ha publicado un comentario en %(groupname)s" % {'name': user.first_name,
                                                                                                    'surname': user.last_name,
                                                                                                    'groupname': group.name}                                                                                          
                group.send_notification(message, settings.VIEW_GROUP) 
                return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
        else:
            form = CommentForm()
        return render_to_response('quaba/groups/comments/new_comment.html',
                                {'form': form, 'group': group},
                                context_instance=RequestContext(request))
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_remove_comment(request, group_id, comment_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        comm = GroupComment.objects.get(group=group, id=comment_id)
        if group.creator.email == user.email or comm.comment.author.email == user.email:
            if 'delete' in request.POST:
                comm.delete()  
                return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
            elif 'no_delete' in request.POST:
                return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
            return render(request, 'quaba/groups/comments/remove_comment.html', {'group': group})  
                                            
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_like_comment(request, group_id, comment_id): 
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    c = GroupComment.objects.get(group=group, id=comment_id)
    if c:
        cv, created = GroupCommentVote.objects.get_or_create(user=user, comment=c)
        if cv.voted:
            return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
        else:
            cv.set_like()
            if user.email != c.comment.author.email:
                message = "A %(name)s %(surname)s le gusta un comentario tuyo en %(groupname)s" % {'name': user.first_name, 
                                                                                                    'surname': user.last_name,
                                                                                                    'groupname': group.name}
                c.comment.author.send_notification(message, settings.VIEW_GROUP, group.id) 
            return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
            
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_comment_likes_list(request, group_id, comment_id):
    group = FriendsGroup.objects.get_group(group_id)
    c = GroupComment.objects.get(group=group, id=comment_id)
    cvs = GroupCommentVote.objects.filter(comment=c)
    like_users = []
    if cvs:
        for cv in cvs:
            like_users.append(cv.user)
    if like_users:
        paginator = Paginator(like_users, 10)
        page = request.GET.get('page')
        try:
            likes = paginator.page(page)
        except PageNotAnInteger:
            likes = paginator.page(1)
        except EmptyPage:
            likes = paginator.page(paginator.num_pages)
    else:
        likes = None
    return render(request, 'quaba/groups/comments/comment_likes.html', {'likes': likes})
                                                    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def remove_from_group(request, email, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    member = UserProfile.objects.get_profile_user(email)
    if group.creator.email == user.email:
        if 'delete' in request.POST:
            Membership.objects.remove(member, group)
            group_message = "%(name)s %(surname)s ha sidom expulsado de %(groupname)s" % {'name': member.first_name,
                                                                                      'surname': member.last_name,
                                                                                      'groupname': group.name}
            group.send_notification(group_message, settings.VIEW_GROUP_MEMBERS)
            return HttpResponseRedirect(reverse('list_group_members', args=(group.id,)))
        elif 'no_delete' in request.POST:
            return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
        return render_to_response('quaba/groups/membership/group_member_remove.html',
                              {'user': member, 'group': group},
                              context_instance=RequestContext(request)) 
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def leave_group(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id) 
    if Membership.objects.is_member(user, group) and user.email != group.creator.email:
        if 'leave' in request.POST:
            Membership.objects.remove(user, group)
            user_message = "Has abandonado %(groupname)s" % {'groupname': group.name}
            user.send_notification(user_message)
            group_message = "%(name)s %(surname)s ha abandonado %(groupname)s" % {'name': user.first_name,
                                                                                    'surname': user.last_name,
                                                                                    'groupname': group.name}
            group.send_notification(group_message, settings.VIEW_GROUP_MEMBERS)
            notifications = GroupNotification.objects.all_notifications(group, user)
            if notifications:
                for n in notifications:
                    n.delete()
            return HttpResponseRedirect(reverse('list_groups'))
        elif 'no_leave' in request.POST:
            return HttpResponseRedirect(reverse('group_view', args=(group_id,)))
        return render(request, 'quaba/groups/membership/group_leave.html', {'group': group}) 

@csrf_protect  
@login_required(login_url='/quaba/login/')
def delete_group(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group) and user.email == group.creator.email:
        if 'delete' in request.POST:
            photobook, c = GroupPhotoBook.objects.get_or_create(group=group)
            pbs = GroupPhoto.objects.photos_for_group(photobook)
            if pbs:
                for pb in pbs:
                    pb.delete()
            photobook.delete()
  
            members = Membership.objects.members_for_group(group)
            for member in members:
                notifications = GroupNotification.objects.all_notifications(group, member)
                if notifications:
                    for n in notifications:
                        n.delete()
                Membership.objects.remove(member, group)
                
            all_comments = GroupComment.objects.filter(group=group)
            for c in all_comments:
                c.delete()
                
            group.delete() 
            user.send_notification('Grupo eliminado con exito')
            return HttpResponseRedirect(reverse('list_groups'))
        elif 'no_delete' in request.POST:
            return HttpResponseRedirect(reverse('group_view', args=(group_id,)))
        return render(request, 'quaba/groups/group_edit/group_delete.html') 
