from django.db.models import Q
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

from quabamv.models import *
from quabamv.forms import *

@csrf_protect  
@login_required(login_url='/quaba/login/')
def friends_menu(request):
    return render(request, 'quaba/friends/menu.html')
     
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_friends(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    friend_list = Friendship.objects.friends_for_user(user)
    if friend_list:
        paginator = Paginator(friend_list, 10)
        page = request.GET.get('page')
        try:
            friends = paginator.page(page)
        except PageNotAnInteger:
            friends = paginator.page(1)
        except EmptyPage:
            friends = paginator.page(paginator.num_pages)
    else:
        friends = None
    return render_to_response('quaba/friends/friends_list.html',
                              {'friends': friends},
                              context_instance=RequestContext(request))
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def user_view(request, email):     
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)  
    if from_user.email == to_user.email:
        return HttpResponseRedirect(reverse('main_menu'))
    notifications = Notification.objects.all_notifications(from_user)
    if notifications:
        for n in notifications:
            if n.type == settings.VIEW_USER and n.parameter_id == to_user.email:
                n.delete()
    if Blocking.objects.get_blocked_user(to_user, from_user):
        return HttpResponseRedirect(reverse('friends_menu'))
    else:
        fdate = None
        if not Friendship.objects.are_friends(from_user, to_user):
            is_friend = False
        else:
            fsh,fshx = Friendship.objects.get_or_create(from_user=from_user, to_user=to_user)
            fdate = fsh.created
            is_friend = True
        ai, aix = UserAdditionalInfo.objects.get_or_create(user=to_user)
        return render_to_response('quaba/friends/user_view.html',
                              {'user': to_user, 'is_friend': is_friend, 'profile_photo': ai.profile_photo, 'fdate': fdate},
                              context_instance=RequestContext(request))
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def user_report(request, email):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)  
    if not Friendship.objects.are_friends(from_user, to_user):
        if 'report' in request.POST:
            to_user.report()
            msg = 'Has reportado a %(name)s %(surname)s' % {'name': to_user.first_name,
                                                            'surname': to_user.last_name}
            from_user.send_notification(msg)
            return HttpResponseRedirect(reverse('main_menu'))
        elif 'no_report' in request.POST:
            return HttpResponseRedirect(reverse('user_view', args=(email,)))
    return render(request, 'quaba/friends/report.html', {'user': to_user})
                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def friend_profile_detail(request, email):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(from_user, to_user):
    
        info,inf = UserAdditionalInfo.objects.get_or_create(user=to_user)
        num_friends = len(Friendship.objects.friends_for_user(to_user))
        num_groups = len(Membership.objects.groups_for_user(to_user))
        num_created_groups = len(FriendsGroup.objects.filter(creator=to_user))
        photobook, pbx = UserPhotoBook.objects.get_or_create(user=to_user)
        pfu = UserPhoto.objects.photos_for_user(photobook)
        if pfu:
            num_photos = len(pfu)
        else:
            num_photos = 0
        
        store, st = UserStore.objects.get_or_create(user=to_user)
        total_size=store.get_size()
        perc = 100*total_size/(5*1024*1024*1024)
        
        num_comments,ncx = UserAllComments.objects.get_or_create(user=to_user)
    
        return render_to_response('quaba/account/detail.html',
                              {'userinfo': info, 'gender': to_user.gender_display(),
                              'userself': False, 'num_comments': num_comments.nc,
                              'num_friends': num_friends, 'num_groups': num_groups, 
                              'num_created_groups': num_created_groups, 
                              'perc': perc, 'num_photos': num_photos, 
                              'store_size': total_size},
                              context_instance=RequestContext(request))
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_friend_friends(request, email):
    user = UserProfile.objects.get_profile_user(request.user.email)
    friend = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(user, friend):
        own_friends = Friendship.objects.friends_for_user(user)
        friend_list = Friendship.objects.friends_for_user(friend)
        
        paginator = Paginator(friend_list,10)
        page = request.GET.get('page')
        try:
            friends = paginator.page(page)
        except PageNotAnInteger:
            friends = paginator.page(1)
        except EmptyPage:
            friends = paginator.page(paginator.num_pages)
        
        return render_to_response('quaba/friends/friends_of_friend.html',
                                 {'friends': friends, 'friend': friend, 'user_friends': own_friends, 'user': user},
                                 context_instance=RequestContext(request))
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def invite_friend(request, email):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    invitation = FriendshipInvitation.objects.filter(to_user=to_user, from_user=from_user)
    if not Friendship.objects.are_friends(from_user, to_user) and not invitation: 
        if 'invite' in request.POST:   
            invitation = FriendshipInvitation(from_user=from_user, to_user=to_user)
            invitation.save()
            from_user_message = "Has enviado una solicitud de amistad a %(name)s %(surname)s." % {'name': to_user.first_name, 
                                                                                                    'surname': to_user.last_name}
            from_user.send_notification(from_user_message)    
            to_user_message = "%(name)s %(surname)s te ha enviado una solicitud de amistad." % {'name': from_user.first_name, 
                                                                                                'surname': from_user.last_name}
            to_user.send_notification(to_user_message, settings.VIEW_USER_INVITATION, invitation.id)   
            return HttpResponseRedirect(reverse('friends_menu'))  
    return render_to_response('quaba/friends/friend_invite.html',
                                 {'user': to_user, 'iv': invitation},
                                 context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def remove_friend(request, email):
    user = UserProfile.objects.get_profile_user(request.user.email)
    friend = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(user, friend):
        if 'delete' in request.POST:  
            Friendship.objects.remove(user, friend) 
            return HttpResponseRedirect(reverse('list_friends'))
        elif 'no_delete' in request.POST:
            return HttpResponseRedirect(reverse('user_view', args=(email,)))
        return render(request, 'quaba/friends/friend_delete.html', {'friend': friend})
        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def block_user(request, email):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    if to_user != from_user:
        blocking = Blocking.objects.filter(from_user=from_user, to_user=to_user)
        if not blocking:
            if 'block' in request.POST:
                blocking = Blocking(from_user=from_user,to_user=to_user)
                blocking.save()
                message = "Has bloqueado a %(name)s %(surname)s." % {'name': to_user.first_name, 
                                                                           'surname': to_user.last_name}
                from_user.send_notification(message) 
                return HttpResponseRedirect(reverse('friends_menu'))
        return render(request, 'quaba/friends/block_user.html', {'user': to_user})
                                      
@csrf_protect  
@login_required(login_url='/quaba/login/')
def unblock_user(request, email):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    blocking = Blocking.objects.get_blocked_user(from_user, to_user)
    if 'delete' in request.POST:
        blocking.delete()
        message = "Has desbloqueado a %(name)s %(surname)s." % {'name': to_user.first_name, 
                                                                           'surname': to_user.last_name}
        from_user.send_notification(message) 
        return HttpResponseRedirect(reverse('friends_menu'))
    elif 'no_delete' in request.POST:
        return HttpResponseRedirect(reverse('list_blocked_users'))
    return render(request, 'quaba/friends/unblock_user.html', {'user': to_user})
    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_blocked_users(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    blocked_list = Blocking.objects.blocked_for_user(user)
    if blocked_list:
        paginator = Paginator(blocked_list, 10)
        page = request.GET.get('page')
        try:
            blocked = paginator.page(page)
        except PageNotAnInteger:
            blocked = paginator.page(1)
        except EmptyPage:
            blocked = paginator.page(paginator.num_pages)
    else:
        blocked = None
    return render_to_response('quaba/friends/blocked_users_list.html',
                              {'blockedusers': blocked},
                              context_instance=RequestContext(request))
                                                  
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_friend_received_invitations(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    invitation_list = FriendshipInvitation.objects.filter(to_user=user)
    if invitation_list:
        paginator = Paginator(invitation_list, 10)
        page = request.GET.get('page')
        try:
            invitations = paginator.page(page)
        except PageNotAnInteger:
            invitations = paginator.page(1)
        except EmptyPage:
            invitations = paginator.page(paginator.num_pages)
    else:
        invitations = None
    return render_to_response('quaba/friends/invitations_list.html',
                              {'invitations': invitations, 'status': 'received'},
                              context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_friend_sent_invitations(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    invitation_list = FriendshipInvitation.objects.filter(from_user=user)
    if invitation_list:
        paginator = Paginator(invitation_list, 10)
        page = request.GET.get('page')
        try:
            invitations = paginator.page(page)
        except PageNotAnInteger:
            invitations = paginator.page(1)
        except EmptyPage:
            invitations = paginator.page(paginator.num_pages)
    else:
        invitations = None
    return render_to_response('quaba/friends/invitations_list.html',
                              {'invitations': invitations, 'status': 'sent'},
                              context_instance=RequestContext(request))
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def delete_friend_invitation(request, invitation_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    invitation = FriendshipInvitation.objects.get_sent_invitation(user, invitation_id)  
    if 'delete' in request.POST:
        invitation.delete()
        notifications = Notification.objects.all_notifications(invitation.to_user)
        if notifications:
            for n in notifications:
                if n.type == settings.VIEW_USER_INVITATION and n.parameter_id == invitation.id:
                    n.delete()
        return HttpResponseRedirect(reverse('friends_sent_invitations'))
    elif 'no_delete' in request.POST:
        return HttpResponseRedirect(reverse('user_view', args=(invitation.to_user.email,)))
    return render(request, 'quaba/friends/invitation_delete.html')

@csrf_protect  
@login_required(login_url='/quaba/login/')
def respond_to_friend_invitation(request, invitation_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    invitation = FriendshipInvitation.objects.get_received_invitation(user, invitation_id)
    notifications = Notification.objects.all_notifications(user)
    if notifications:
        for n in notifications:
            if n.type == settings.VIEW_USER_INVITATION and n.parameter_id == invitation_id:
                n.delete()
    if 'accept' in request.POST:
        invitation.accept()
        user_message = "Ahora %(name)s %(surname)s y tu sois amigos." %  {'name': invitation.from_user.first_name, 
                                                                          'surname': invitation.from_user.last_name}
        user.send_notification(user_message)                                                                   
        friend_message = "%(name)s %(surname)s ha aceptado tu peticion de amistad." %  {'name': user.first_name, 
                                                                                        'surname': user.last_name}
        invitation.from_user.send_notification(friend_message, settings.VIEW_USER, user.email) 
        return HttpResponseRedirect(reverse('list_friends'))
    elif 'decline' in request.POST:
        invitation.decline()
        user_message = "%(name)s %(surname)s ha sido bloqueado para enviarte mas solicitudes de amistad." %  {'name': invitation.from_user.first_name, 
                                                                                                                'surname': invitation.from_user.last_name}
        user.send_notification(user_message) 
        return HttpResponseRedirect(reverse('friends_menu'))
    return render(request, 'quaba/friends/invitation_show.html', {'user': invitation.from_user})

    