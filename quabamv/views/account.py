# -*- coding: utf-8 -*-

from django.db.models import Q
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User

from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required

from django.conf import settings
from django.core.mail import EmailMessage

from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.contrib import messages 

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

from quabamv.models import *
from quabamv.forms import *

@csrf_protect
def about(request):
    return render(request, 'quaba/about.html')

@csrf_protect    
def welcome(request):
    if 'login' in request.POST:
        return HttpResponseRedirect(reverse('log_in'))
    elif 'register' in request.POST:
        return HttpResponseRedirect(reverse('register'))
    elif 'about' in request.POST:
        return HttpResponseRedirect(reverse('about'))
    return render(request, 'quaba/welcome.html')

@csrf_protect
def register(request):  
    registered = False
    name = 'no_name'
    if request.method == 'POST':
        form = UserProfileForm(request.POST) 
        
        if form.is_valid():
            u = UserProfile.objects.filter(Q(email=form.cleaned_data['email'])|Q(dni=form.cleaned_data['dni']))
            if u:
                message = 'ERROR. La cuenta con el email o DNI especificados ya existe. Inténtalo de nuevo.'
                return render(request, 'quaba/error/auth_error.html',  {'register': True, 'msg': message})
            else:
                user = form.save()
            
                username = UserProfile.objects.generate_random_username(user.dni)
                u = User.objects.create_user(username, user.email, user.password)
                u.is_active = True
                u.save()
            
                name = user.first_name        
                registered = True   
            
                #INICIALIZAR INFORMACION ADICIONAL DE USUARIO
                info = UserAdditionalInfo.objects.get_or_create(user=user)
            
                #INICIALIZAR LIBRO DE FOTOS
                photobook = UserPhotoBook.objects.get_or_create(user=user)
            
                #INICIALIZAR ALMACEN DE ARCHIVOS
                store = UserStore.objects.get_or_create(user=user)
                
                numc = UserAllComments.objects.get_or_create(user=user)    
        else:
            message = 'ERROR. Has Introducido Datos No Validos. Intentalo de nuevo.'
            return render(request, 'quaba/error/auth_error.html',  {'register': True, 'msg': message})
    else:
        form = UserProfileForm()
            
    return render_to_response('quaba/account/register.html',
                             {'user_form': form, 'registered': registered, 'name': name},
                             context_instance=RequestContext(request))

@csrf_protect
def log_in(request):   
    if 'email' in request.GET:
        q1 = request.GET['email']
        if 'password' in request.GET:
            q2 = request.GET['password']
            try:
                u = UserProfile.objects.get(email=q1)
                user, usx = User.objects.get_or_create(email=u.email)
                auth_user = authenticate(username=user.username, password=q2)
                if not auth_user:
                    message = 'ERROR. Email o contraseña incorrectos. Inténtalo de nuevo.'
                    return render(request, 'quaba/error/auth_error.html',  {'register': False, 'msg': message})    
                if auth_user.is_authenticated():
                    logout(request)
                login(request, auth_user)
                return HttpResponseRedirect(reverse('main_menu'))
            except UserProfile.DoesNotExist:
                message = 'ERROR. No existe ninguna cuenta con el email especificado. Intentalo de nuevo.'
                return render(request, 'quaba/error/auth_error.html',  {'register': False, 'msg': message})              
        else:
            message = 'ERROR. Has introducido datos no válidos. Intentalo de nuevo.'
            return render(request, 'quaba/error/auth_error.html',  {'register': False, 'msg': message})
    return render(request, 'quaba/account/login.html')
    
@csrf_protect    
def password_recovery(request):
    error = False
    done = False
    if 'dni' in request.GET:
        done = True
        q = request.GET['dni']
        try:
            us = UserProfile.objects.get(dni=q)               
            subject = 'Clave de acceso a Quaba. Recordatorio'  
            message = "Hola, %(name)s. Tu clave de acceso a Quaba es %(pwd)s" % {'name': us.first_name,
                                                                                        'pwd': us.password}
            msg = EmailMessage(subject, message, to=[us.email])
            msg.send()
        except UserProfile.DoesNotExist:
            error = True
            return HttpResponseRedirect(reverse('error_404'))
    else:
        error = True
    return render(request, 'quaba/account/recovery.html', {'error': error, 'done': done})
             
@csrf_protect             
@login_required(login_url='/quaba/login/')                                 
def main_menu(request): 
    user = UserProfile.objects.get_profile_user(request.user.email)
    notifications = Notification.objects.all_notifications(user)
    if notifications:
        if 'notif_clean' in request.POST:
            notifications.delete()
            return HttpResponseRedirect(reverse('main_menu'))
        for notif in notifications:
            if 'notif_delete' in request.POST:
                notif.delete()
                return HttpResponseRedirect(reverse('main_menu'))
    group_notifications = GroupNotification.objects.filter(to_user=user)
    if group_notifications:
        if 'notif_clean' in request.POST:
            group_notifications.delete()
            return HttpResponseRedirect(reverse('main_menu'))
        for notif in group_notifications:
            if 'notif_delete' in request.POST:
                notif.delete()
                return HttpResponseRedirect(reverse('main_menu'))
    ai,aix = UserAdditionalInfo.objects.get_or_create(user=user)
    
    return render(request, 'quaba/account/main_menu.html',  {'user': user, 
                                                            'notifications': notifications, 
                                                            'group_notifications': group_notifications,
                                                            'profile_photo': ai.profile_photo})

@csrf_protect                                                            
@login_required(login_url='/quaba/login/')
def log_out(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    notifications = Notification.objects.all_notifications(user)
    if notifications:
        for n in notifications:
            n.delete()
    context = {}
    context.update(csrf(request))
    logout(request)
    return HttpResponseRedirect(reverse('main_menu'))
    
@csrf_protect                                                            
@login_required(login_url='/quaba/login/')
def settings_menu(request): 
    return render(request,'quaba/account/settings_view.html')
    
@csrf_protect                                                            
@login_required(login_url='/quaba/login/')
def edit_profile(request): 
    user = UserProfile.objects.get_profile_user(request.user.email)
    if request.method == 'POST':
        form = AdditionalInfoForm(user, request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('settings_menu'))
    else:
        form = AdditionalInfoForm(user)
    return render_to_response('quaba/account/edit_profile.html',
                             {'form': form},
                             context_instance=RequestContext(request))        
                             
@csrf_protect                                                            
@login_required(login_url='/quaba/login/') 
def password_modify(request):   
    user = UserProfile.objects.get_profile_user(request.user.email)
    if request.method == 'POST':
        form = UserNewPasswordForm(user, request.POST)
        if form.is_valid():
            user = UserProfile.objects.get_profile_user(request.user.email)
            pwd = form.cleaned_data['new_password']
            user.password = pwd
            user.save()
            
            request.user.set_password(pwd)
            request.user.save()

            return HttpResponseRedirect(reverse('settings_menu'))
    else:
        form = UserNewPasswordForm(user)
    return render_to_response('quaba/account/password_modify.html',
                                 {'form': form},
                                 context_instance=RequestContext(request))
                                
@csrf_protect                                                            
@login_required(login_url='/quaba/login/')
def profile_detail(request):
    user = UserProfile.objects.get_profile_user(request.user.email)   
    info = UserAdditionalInfo.objects.get(user=user)
    
    num_comments,ncx = UserAllComments.objects.get_or_create(user=user)
        
    num_friends = len(Friendship.objects.friends_for_user(user))
    num_groups = len(Membership.objects.groups_for_user(user))
    num_created_groups = len(FriendsGroup.objects.filter(creator=user))
    photobook,x = UserPhotoBook.objects.get_or_create(user=user)
    pfu = UserPhoto.objects.photos_for_user(photobook)
    if pfu:
        num_photos = len(UserPhoto.objects.photos_for_user(photobook))
    else:
        num_photos = 0
    
    store, c = UserStore.objects.get_or_create(user=user)
    total_size = store.get_size()
    perc = 100*total_size/(5*1024*1024*1024)

    return render_to_response('quaba/account/detail.html',
                              {'userinfo': info, 'gender': user.gender_display(),
                              'num_friends': num_friends,'num_groups': num_groups,
                              'num_created_groups': num_created_groups, 'perc': perc,
                              'userself': True, 'num_comments': num_comments.nc,
                              'num_photos': num_photos,'store_size': total_size},
                              context_instance=RequestContext(request))
                              
@csrf_protect                                                            
@login_required(login_url='/quaba/login/')
def sign_down(request): 
    user = UserProfile.objects.get_profile_user(request.user.email)
    sign_down_attempted = False
    sign_down_done = False  
    
    if 'sign_down' in request.POST: #se ha pulsado el boton   
        context = {}
        context.update(csrf(request))
        logout(request)
        
        sign_down_attempted = True
        sign_down_done = True

        notifications = Notification.objects.all_notifications(user)
        if notifications:
            for n in notifications:
                n.delete()
        
        #ELIMINAR ARCHIVOS Y CARPETAS DEL USUARIO
        store, c1 = UserStore.objects.get_or_create(user=user)
        folders = UserFolder.objects.all_folders(store)
        if folders:
            folders.delete()
        files = UserFile.objects.all_files(store)
        if files:
            files.delete() 
        store.delete()
        
        #ELIMINAR FOTOS DEL USUARIO
        photobook, c2 = UserPhotoBook.objects.get_or_create(user=user)
        photos = UserPhoto.objects.photos_for_user(photobook)
        if photos:
            photos.delete()
        photobook.delete()
            
        #ELIMINAR O DEJAR DE SER MIEMBRO DE GRUPOS
        groups = Membership.objects.groups_for_user(user)
        for group in groups:
            if group.creator.email == user.email:
                mbs = Membership.objects.members_for_group(group)
                for mb in mbs:
                    notifications = Notification.objects.all_notifications(mb)
                    if notifications:
                        for n in notifications:
                            n.delete()
                group.delete()
            else:
                member = Membership.objects.filter(member=user, group=group)
                if member:
                    member.delete()
            
        #ELIMINAR RELACIONES DE AMISTAD            
        friends = Friendship.objects.friends_for_user(user)
        for friend in friends:
            notifications = Notification.objects.all_notifications(friend)
            if notifications:
                for n in notifications:
                    n.delete()
            mss = Message.objects.all_messages(user, friend)
            mss.delete()
            Friendship.objects.remove(user, friend)
               
        #CERRAR SESION Y BORRAR USUARIO
        info = UserAdditionalInfo.objects.get(user=user)
        info.delete()
        
        u = User.objects.get(email=user.email)
        u.delete()

        user.delete()  
        
    elif 'not_sign_down' in request.POST:
        sign_down_attempted = True

    return render(request, 'quaba/account/sign_down.html',
                                  {'attempted': sign_down_attempted,
                                  'done': sign_down_done})