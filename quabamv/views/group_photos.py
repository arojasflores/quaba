from django.db.models import Q
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from datetime import datetime

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.conf import settings

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

from quabamv.models import *
from quabamv.forms import *
    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_photobook_view(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        photobook, px = GroupPhotoBook.objects.get_or_create(group=group)
        photo_list = GroupPhoto.objects.photos_for_group(photobook)
        if photo_list:
            paginator = Paginator(photo_list, 10)
            page = request.GET.get('page')
            try:
                photos = paginator.page(page)
            except PageNotAnInteger:
                photos = paginator.page(1)
            except EmptyPage:
                photos = paginator.page(paginator.num_pages)
        else:
            photos = None
        return render_to_response('quaba/groups/media/photobook_view.html',
                                 {'photos': photos, 'group': group},
                                 context_instance=RequestContext(request))
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_filter_photos(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        photobook, px = GroupPhotoBook.objects.get_or_create(group=group)
        if 'year' in request.GET and 'month' in request.GET:
            year = request.GET['year']
            month = request.GET['month']
            photo_list = UserPhoto.objects.photos_by_date(photobook, year, month)  
            if photo_list:
                paginator = Paginator(photo_list, 10)
                page = request.GET.get('page')
                try:
                    photos = paginator.page(page)
                except PageNotAnInteger:
                    photos = paginator.page(1)
                except EmptyPage:
                    photos = paginator.page(paginator.num_pages)
            else:
                photos = None
            return render_to_response('quaba/search/search_photo_results.html',
                                        {'photos': photos, 'friend': None, 'group': group,
                                        'date': Misc.objects.date_name(year, month)},
                                        context_instance=RequestContext(request))
    return render(request, 'quaba/search/search_photo_form.html')           
                                                                                                               
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_photo_view(request, group_id, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        photobook, pbx = GroupPhotoBook.objects.get_or_create(group=group)
        photo = GroupPhoto.objects.get_photo(photobook, photo_id)
        comm_list = GroupPhotoComment.objects.filter(photo=photo)
        if comm_list:
            paginator = Paginator(comm_list, 10)
            page = request.GET.get('page')
            try:
                comm = paginator.page(page)
            except PageNotAnInteger:
                comm = paginator.page(1)
            except EmptyPage:
                comm = paginator.page(paginator.num_pages)
        else:
            comm = None
        notifications = GroupNotification.objects.all_notifications(group, user)
        if notifications:
            for n in notifications:
                if n.type == settings.VIEW_GROUP_PHOTO and n.group.id == group.id and n.parameter_id == photo.id:
                    n.delete()
        if photo.creator.email != user.email:
            pcr = False
        else:
            pcr = True
        return render_to_response('quaba/groups/media/photo_view.html',
                                 {'group': group, 'photo': photo, 'pcr': pcr, 'comm': comm},
                                 context_instance=RequestContext(request))
                                                                                                   
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_upload_photo(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        if request.method == 'POST':
            form = GroupPhotoForm(request.POST, request.FILES)
            if form.is_valid():
                photo = form.save(user, group)
                message = "%(name)s %(surname)s ha agregado una foto a %(gname)s." % {'name': user.first_name,
                                                                                        'surname': user.last_name,
                                                                                        'gname': group.name} 
                group.send_notification(message, settings.VIEW_GROUP_PHOTO, photo.id)
                return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
        else:
            form = GroupPhotoForm()
        return render_to_response('quaba/groups/media/photo_upload.html',
                                 {'form': form, 'group': group},
                                 context_instance=RequestContext(request))
                               
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_rename_photo(request, group_id, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        photobook, pbx = GroupPhotoBook.objects.get_or_create(group=group)
        photo = GroupPhoto.objects.get_photo(photobook, photo_id)
        if request.method == 'POST':
            form = TitleForm(request.POST)
            if form.is_valid():
                old_name = photo.title
                new_name = form.cleaned_data['title']
                if new_name != old_name:
                    photo.title = new_name
                    photo.save()
                    message = "%(name)s %(surname)s ha modificado el nombre de una foto en %(gname)s." % {'name': user.first_name,
                                                                                                            'surname': user.last_name,
                                                                                                            'gname': group.name}                                                                         
                    group.send_notification(message, settings.VIEW_GROUP_PHOTO, photo.id)
                    return HttpResponseRedirect(reverse('group_photo_view', args=(group_id, photo_id,)))
                else:
                    return HttpResponseRedirect(reverse('group_rename_photo', args=(group_id, photo_id,)))
        else:
            form = TitleForm(initial={'title': photo.title})
        return render_to_response('quaba/groups/media/photo_rename.html',
                              {'form': form, 'photo': photo, 'group': group},
                              context_instance=RequestContext(request))
     
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_delete_photo(request, group_id, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        photobook, pbx = GroupPhotoBook.objects.get_or_create(group=group)
        photo = GroupPhoto.objects.get_photo(photobook, photo_id)
        if 'delete' in request.POST:
            gmp,x = GroupMainPhoto.objects.get_or_create(group=group)
            if not gmp.photo or gmp.photo.id == photo.id:
                photo.delete()                                                             
            else:
                message = "Esta foto es la foto principal de %(gname)s y no puede eliminarse." % {'gname': group.name}
                user.send_notification(message)
            return HttpResponseRedirect(reverse('group_photobook_view', args=(group.id,)))
        elif 'no_delete' in request.POST:
            return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
        return render(request, 'quaba/groups/media/photo_delete.html')
        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_main_photo(request, group_id, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        photobook, pbx = GroupPhotoBook.objects.get_or_create(group=group)
        photo = GroupPhoto.objects.get_photo(photobook, photo_id)
        gmp,x = GroupMainPhoto.objects.get_or_create(group=group)
        gmp.photo = photo
        gmp.save()
        message = "%(name)s %(surname)s ha cambiado la foto principal de %(gname)s." % {'name': user.first_name,
                                                                                        'surname': user.last_name,
                                                                                        'gname': group.name} 
        group.send_notification(message, settings.VIEW_GROUP_PHOTO, photo.id)
        return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_like_photo(request, group_id, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    photobook, pbx = GroupPhotoBook.objects.get_or_create(group=group)
    photo = GroupPhoto.objects.get_photo(photobook, photo_id)
    if Membership.objects.is_member(user, group):
        pv, pvx = GroupPhotoVote.objects.get_or_create(user=user, photo=photo)
        if pv.voted:
            return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
        else:    
            pv.set_like() 
            if photo.creator.email != user.email:
                message = "A %(name)s %(surname)s le gusta una foto tuya de %(groupname)s" % {'name': user.first_name,
                                                                             'surname': user.last_name,
                                                                             'groupname': group.name}                                                                                                                                              
                photo.creator.send_notification(message, settings.VIEW_GROUP_PHOTO, group.id, photo.id)
            return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
            
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_photo_likes_list(request, group_id, photo_id):
    group = FriendsGroup.objects.get_group(group_id)
    photobook, pbx = GroupPhotoBook.objects.get_or_create(group=group)
    photo = GroupPhoto.objects.get_photo(photobook, photo_id)
    pvs = GroupPhotoVote.objects.filter(photo=photo)
    like_users = []
    if pvs:
        for pv in pvs:
            like_users.append(pv.user)
    if like_users:
        paginator = Paginator(like_users, 10)
        page = request.GET.get('page')
        try:
            likes = paginator.page(page)
        except PageNotAnInteger:
            likes = paginator.page(1)
        except EmptyPage:
            likes = paginator.page(paginator.num_pages)
    else:
        likes = None
    return render(request, 'quaba/groups/media/likes.html', {'likes': likes})

@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_photo_new_comment(request, group_id, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    photo = GroupPhoto.objects.get_photo(group, photo_id)
    if Membership.objects.is_member(user, group):
        if request.method == 'POST':
            form = CommentForm(request.POST)
            if form.is_valid():
                c = form.save(user)
                comment = GroupPhotoComment(photo=photo, comment=c)
                comment.save()
                
                numc = UserAllComments.objects.get_or_create(user=user)
                numc.inc()
                
                msg = "%(name)s %(surname)s ha comentado una foto" % {'name': user.first_name, 'surname': user.last_name}
                group.send_notification(msg, settings.GROUP_PHOTO_VIEW, photo.id)
                return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
        else:
            form = CommentForm()
        return render_to_response('quaba/groups/media/new_comment.html',
                                {'form': form, 'photo': photo, 'group': group},
                                context_instance=RequestContext(request)) 
                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_photo_delete_comment(request, group_id, photo_id, comment_id): 
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    photo = GroupPhoto.objects.get_photo(group, photo_id)
    comm = GroupPhotoComment.objects.get(photo=photo, id=comment_id)
    if Membership.objects.is_member(user, group):
        if photo.creator.email == user.email or comm.comment.author.email == user.email:
            if 'delete' in request.POST:
                comm.delete()
                return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
            elif 'no_delete' in request.POST:
                return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
        return render(request, 'quaba/groups/media/remove_comment.html') 
    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_photo_like_comment(request, group_id, photo_id, comment_id): 
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    photo = GroupPhoto.objects.get_photo(group, photo_id)
    c = GroupPhotoComment.objects.get(photo=photo, id=comment_id)
    if c:
        cv,cvx = GroupPhotoCommentVote.objects.get_or_create(user=user, comment=comm)
        if cv.voted:
            return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
        else:
            cv.set_like()
            if user.email != c.comment.author.email:
                like_message = "A %(name)s %(surname)s le gusta un comentario tuyo en %(title)s, en el grupo %(gname)s" % {'name': user.first_name,
                                                                                                                    'surname': user.last_name,
                                                                                                                    'title': photo.title, 
                                                                                                                    'gname': group.name}
                c.comment.author.send_notification(like_message, settings.GROUP_PHOTO_VIEW, group.id, photo.id) 
            return HttpResponseRedirect(reverse('group_photo_view', args=(group.id, photo.id,)))
            
@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_photo_comment_likes_list(request, group_id, photo_id, comment_id):
    group = FriendsGroup.objects.get_group(group_id)
    photo = GroupPhoto.objects.get_photo(group, photo_id)
    c = GroupPhotoComment.objects.get(photo=photo, id=comment_id)
    cvs = UserPhotoCommentVote.objects.filter(comment=c)
    like_users = []
    if cvs:
        for cv in cvs:
            like_users.append(cv.user)
    if like_users:
        paginator = Paginator(like_users, 10)
        page = request.GET.get('page')
        try:
            likes = paginator.page(page)
        except PageNotAnInteger:
            likes = paginator.page(1)
        except EmptyPage:
            likes = paginator.page(paginator.num_pages)
    else:
        likes = None
    return render(request, 'quaba/groups/media/comment_likes.html', {'likes': likes})