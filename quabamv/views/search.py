from django.db.models import Q
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib import messages 

from django.core.exceptions import PermissionDenied
from django.http import Http404

from django.conf import settings

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

from quabamv.models import *

@csrf_protect  
@login_required(login_url='/quaba/login/')
def search_user(request):
    sel_age = False
    sel_cnt = False
    sel_twn = False
    sel_ntw = False
    
    user = UserProfile.objects.get_profile_user(request.user.email)
    us = UserProfile.objects.all()
    blocked = Blocking.objects.blocked_for_user(user)
    for u in us:
        if u in blocked:
            us = us.exclude(email=u.email)
    us = us.exclude(email=user.email)
    user_ai = UserAdditionalInfo.objects.get(user=user)
    for u in us:
        if 'selected_age' in request.GET:
            sel_age = True
            if user.age != u.age:
                us = us.exclude(email=u.email)
        ai, created = UserAdditionalInfo.objects.get_or_create(user=u)
        if 'selected_country' in request.GET:
            sel_cnt = True
            if user_ai.country != ai.country:
                us = us.exclude(email=u.email)
        if 'selected_town' in request.GET and user_ai.town:
            sel_twn = True
            if user_ai.town and user_ai.town != ai.town:
                us = us.exclude(email=u.email)
        if 'selected_network' in request.GET:
            sel_ntw = True
            if user_ai.network and user_ai.network != ai.network:
                us = us.exclude(email=u.email)
                
    if 'q' in request.GET:
        q = request.GET['q']
        if q:
            for term in q.split():
                us = us.filter(Q(first_name__icontains=term)|Q(last_name__icontains=term)) 
        if us:
            paginator = Paginator(us, 10)
            page = request.GET.get('page')
            try:
                users = paginator.page(page)
            except PageNotAnInteger:
                users = paginator.page(1)
            except EmptyPage:
                users = paginator.page(paginator.num_pages)
        else:
            users = None
        return render(request, 'quaba/search/search_user_results.html', 
                            {'users': users, 'query': q, 'ai': user_ai,
                            'friends': Friendship.objects.friends_for_user(user),
                            'sel_age': sel_age, 'sel_cnt': sel_cnt, 'sel_twn': sel_twn, 'sel_ntw': sel_ntw})
    return render(request, 'quaba/search/search_user_form.html')
        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def search_friend(request):
    sel_age = False
    sel_cnt = False
    sel_twn = False
    sel_ntw = False
    
    user = UserProfile.objects.get_profile_user(request.user.email)
    
    all_users = UserProfile.objects.all()
    all_users = all_users.exclude(email = user.email)
    
    for u in all_users:
        if not Friendship.objects.are_friends(user, u):
            all_users = all_users.exclude(email=u.email)
    
    user_ai,c = UserAdditionalInfo.objects.get_or_create(user=user)
    for u in all_users:
        if 'selected_age' in request.GET:
            sel_age = True
            if user.age != u.age:
                all_users = all_users.exclude(email=u.email)
        ai, created = UserAdditionalInfo.objects.get_or_create(user=u)
        if 'selected_country' in request.GET:
            sel_cnt = True
            if user_ai.country != ai.country:
                all_users = all_users.exclude(email=u.email)
        if 'selected_town' in request.GET and user_ai.town:
            sel_twn = True
            if user_ai.town != ai.town:
                all_users = all_users.exclude(email=u.email)
        if 'selected_network' in request.GET:
            sel_ntw = True
            if user_ai.network != ai.network:
                all_users = all_users.exclude(email=u.email)
    if 'q' in request.GET:
        q = request.GET['q']
        if q:
            for term in q.split():
                all_users = all_users.filter(Q(first_name__icontains=term)|Q(last_name__icontains=term))
        if all_users:
            paginator = Paginator(all_users, 10)
            page = request.GET.get('page')
            try:
                users = paginator.page(page)
            except PageNotAnInteger:
                users = paginator.page(1)
            except EmptyPage:
                users = paginator.page(paginator.num_pages)
        else:
            users = None
        return render(request, 'quaba/search/search_user_results.html',
                {'users': users, 'query': q, 'ai': user_ai, 'friends': None,
                    'sel_age': sel_age, 'sel_cnt': sel_cnt, 'sel_twn': sel_twn, 
                    'sel_ntw': sel_ntw})
    return render(request, 'quaba/search/search_user_form.html')

@csrf_protect  
@login_required(login_url='/quaba/login/')
def search_friend_of_friend(request, email):
    sel_age = False
    sel_cnt = False
    sel_twn = False
    sel_ntw = False
    
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(from_user, to_user):
        users = UserProfile.objects.all()
        users = users.exclude(email = from_user.email)
        for u in users:
            if not Friendship.objects.are_friends(u, to_user):
                users = users.exclude(email=u.email)
        user_ai, c = UserAdditionalInfo.objects.get_or_create(user=from_user)
        for u in users:
            if 'selected_age' in request.GET:
                sel_age = True
                if from_user.age != u.age:
                    users = users.exclude(email=u.email)
            ai, created = UserAdditionalInfo.objects.get_or_create(user=u)
            if 'selected_country' in request.GET:
                sel_cnt = True
                if user_ai.country != ai.country:
                    users = users.exclude(email=u.email)
            if 'selected_town' in request.GET and user_ai.town:
                sel_twn = True
                if user_ai.town != ai.town:
                    users = users.exclude(email=u.email)
            if 'selected_network' in request.GET:
                sel_ntw = True
                if user_ai.network != ai.network:
                    users = users.exclude(email=u.email)
        if 'q' in request.GET:
            q = request.GET['q']
            if q:
                for term in q.split():
                    users = users.filter(Q(first_name__icontains=term)|Q(last_name__icontains=term))
            if users:
                paginator = Paginator(users, 10)
                page = request.GET.get('page')
                try:
                    friends = paginator.page(page)
                except PageNotAnInteger:
                    friends = paginator.page(1)
                except EmptyPage:
                    friends = paginator.page(paginator.num_pages)
            else:
                friends = None
            return render(request, 'quaba/search/search_user_results.html',
                    {'users': friends, 'query': q, 'ai': user_ai,
                        'friends': Friendship.objects.friends_for_user(from_user),
                        'sel_age': sel_age, 'sel_cnt': sel_cnt, 'sel_twn': sel_twn, 
                        'sel_ntw': sel_ntw})
        return render(request, 'quaba/search/search_user_form.html')
                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def search_group(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    groups = FriendsGroup.objects.all()
    if 'q' in request.GET:
        q = request.GET['q']
        if q:
            for term in q.split():
                groups = groups.filter(name__icontains=term)
        if groups:
            paginator = Paginator(groups, 10)
            page = request.GET.get('page')
            try:
                gs = paginator.page(page)
            except PageNotAnInteger:
                gs = paginator.page(1)
            except EmptyPage:
                gs = paginator.page(paginator.num_pages)
        else:
            gs = None
        return render(request, 'quaba/search/search_group_results.html',
                {'groups': gs, 'query': q, 'user': user,
                'user_groups': Membership.objects.groups_for_user(user)})
    return render(request, 'quaba/search/search_group_form.html')
        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def search_user_group(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    groups = FriendsGroup.objects.all()
    for g in groups:
        if not Membership.objects.is_member(user, g):
            groups = groups.exclude(name=g.name)
    if 'q' in request.GET:
        q = request.GET['q']
        if q:
            for term in q.split():
                groups = groups.filter(name__icontains=term)
        if groups:
            paginator = Paginator(groups, 10)
            page = request.GET.get('page')
            try:
                gs = paginator.page(page)
            except PageNotAnInteger:
                gs = paginator.page(1)
            except EmptyPage:
                gs = paginator.page(paginator.num_pages)
        else:
            gs = None
        return render(request, 'quaba/search/search_group_results.html',
                {'groups': gs, 'query': q, 'user_groups': None, 'user': user})
    return render(request, 'quaba/search/search_group_form.html')
        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def search_friend_group(request, email):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(from_user, to_user):
        groups = FriendsGroup.objects.all()
        for g in groups:
            if not Membership.objects.is_member(to_user, g):
                groups = groups.exclude(name=g.name)
        if 'q' in request.GET:
            q = request.GET['q']
            if q:
                for term in q.split():
                    groups = groups.filter(name__icontains=term)
        if groups:
            paginator = Paginator(groups, 10)
            page = request.GET.get('page')
            try:
                gs = paginator.page(page)
            except PageNotAnInteger:
                gs = paginator.page(1)
            except EmptyPage:
                gs = paginator.page(paginator.num_pages)
        else:
            gs = None         
            return render(request, 'quaba/search/search_group_results.html',
                    {'groups': groups, 'query': q, 'user': user,
                    'user_groups': Membership.objects.groups_for_user(from_user)})
        return render(request, 'quaba/search/search_group_form.html')
        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def search_group_member(request, group_id):
    sel_age = False
    sel_cnt = False
    sel_twn = False
    sel_ntw = False
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if Membership.objects.is_member(user, group):
        us = UserProfile.objects.all()
        us = us.exclude(email=user.email)
        for u in us:
            if not Membership.objects.is_member(u, group):
                us = us.exclude(email=u.email)
        user_ai, c = UserAdditionalInfo.objects.get_or_create(user=user)
        for u in us:
            if 'selected_age' in request.GET:
                sel_age = True
                if user.age != u.age:
                    us = us.exclude(email=u.email)
            ai, created = UserAdditionalInfo.objects.get_or_create(user=u)
            if 'selected_country' in request.GET:
                sel_cnt = True
                if user_ai.country != ai.country:
                    us = us.exclude(email=u.email)
            if 'selected_town' in request.GET and user_ai.town:
                sel_twn = True
                if user_ai.town != ai.town:
                    us = us.exclude(email=u.email)
            if 'selected_network' in request.GET:
                sel_ntw = True
                if user_ai.network != ai.network:
                    us = us.exclude(email=u.email)     
        if 'q' in request.GET:
            q = request.GET['q']
            if q:
                for term in q.split():
                    us = us.filter(Q(first_name__icontains=term)|Q(last_name__icontains=term))
            if us:
                paginator = Paginator(us, 10)
                page = request.GET.get('page')
                try:
                    users = paginator.page(page)
                except PageNotAnInteger:
                    users = paginator.page(1)
                except EmptyPage:
                    users = paginator.page(paginator.num_pages)
            else:
                users = None
            return render(request, 'quaba/search/search_user_results.html',
                    {'users': users, 'query': q, 'ai': user_ai,
                        'friends': Friendship.objects.friends_for_user(user),
                        'sel_age': sel_age, 'sel_cnt': sel_cnt, 'sel_twn': sel_twn, 
                        'sel_ntw': sel_ntw})
        return render(request, 'quaba/search/search_user_form.html')  