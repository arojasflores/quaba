from django.db.models import Q
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.conf import settings
import datetime

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

from quabamv.models import *
from quabamv.forms import *

@csrf_protect  
@login_required(login_url='/quaba/login/')
def inbox(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    message_list = Message.objects.inbox_for(user)
    if message_list:
        paginator = Paginator(message_list, 10)
        page = request.GET.get('page')
        try:
            inbox = paginator.page(page)
        except PageNotAnInteger:
            inbox = paginator.page(1)
        except EmptyPage:
            inbox = paginator.page(paginator.num_pages)
    else:
        inbox = None
    return render_to_response('quaba/messages/inbox.html', 
                                {'message_list': inbox}, 
                                context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def outbox(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    message_list = Message.objects.outbox_for(user)
    if message_list:
        paginator = Paginator(message_list, 10)
        page = request.GET.get('page')
        try:
            outbox = paginator.page(page)
        except PageNotAnInteger:
            outbox = paginator.page(1)
        except EmptyPage:
            outbox = paginator.page(paginator.num_pages)
    else:
        outbox = None
    return render_to_response('quaba/messages/outbox.html', 
                                {'message_list': outbox}, 
                                context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def compose(request, email):
    send_from = UserProfile.objects.get_profile_user(request.user.email)
    send_to = UserProfile.objects.get_profile_user(email)
    
    if Friendship.objects.are_friends(send_from, send_to):
        if request.method == "POST":
            form = MessageForm(request.POST)
            if form.is_valid():
                form.save(send_from, send_to)
                sender_message = "Mensaje enviado a %(name)s %(surname)s con exito." % {'name': send_to.first_name,
                                                                            'surname': send_to.last_name}
                send_from.send_notification(sender_message) 
                recipient_message = "%(name)s %(surname)s te ha enviado un mensaje." % {'name': send_from.first_name,
                                                                            'surname': send_from.last_name}
                send_to.send_notification(recipient_message, settings.VIEW_MESSAGES)
                return HttpResponseRedirect(reverse('messages_inbox'))
        else:
            form = MessageForm()
        return render_to_response('quaba/messages/compose.html',
                {'form': form, 'response': False, 'user': send_to},
                context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def reply(request, message_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    message = Message.objects.get_message(message_id)
    if not message.replied and Friendship.objects.are_friends(message.send_from, user):
        if request.method == "POST":
            form = MessageForm(request.POST)
            if form.is_valid():
                msg = form.save(user, message.send_from, message)
                ns = Notification.objects.all_notifications(user)
                if ns:
                    for n in ns:
                        if n.type == settings.VIEW_MESSAGES:
                            n.delete()
                user_message = "Mensaje respondido a %(name)s %(surname)s con exito" % {'name': msg.send_to.first_name, 
                                                                                        'surname': msg.send_to.last_name}
                msg.send_from.send_notification(user_message) 
                recipient_message = "%(name)s %(surname)s te ha respondido a un mensaje." % {'name': msg.send_from.first_name,
                                                                                            'surname': msg.send_from.last_name}
                msg.send_to.send_notification(recipient_message, settings.VIEW_MESSAGES)
                return HttpResponseRedirect(reverse('messages_outbox'))
        else:
            form = MessageForm()
        return render_to_response('quaba/messages/compose.html',
                {'form': form, 'response': True, 'user': message.send_from},
                context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def detail(request, message_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    message = Message.objects.get_message(message_id)
    ns = Notification.objects.all_notifications(message.send_to)
    if ns:
        for n in ns:
            if n.type == settings.VIEW_MESSAGES:
                n.delete()
    if not message.read and message.send_to.email == user.email:
        message.read = True
        message.save()
    return render_to_response('quaba/messages/view.html', 
                                {'message': message},
                                context_instance=RequestContext(request))
       
@csrf_protect  
@login_required(login_url='/quaba/login/')
def delete(request, message_id):
    message = Message.objects.get_message(message_id)
    if 'delete' in request.POST:
        message.delete()
        ns = Notification.objects.all_notifications(message.send_to)
        if ns:
            for n in ns:
                if n.type == VIEW_MESSAGES:
                    n.delete() 
        return HttpResponseRedirect(reverse('messages_inbox'))
    elif 'no_delete' in request.POST:
        return HttpResponseRedirect(reverse('messages_detail', args=(message_id,)))
    return render(request, 'quaba/messages/delete.html') 

@csrf_protect  
@login_required(login_url='/quaba/login/')
def friend_messages(request, email):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(from_user, to_user):
        message_list = Message.objects.all_messages(from_user, to_user)
        if message_list:
            paginator = Paginator(message_list, 10)
            page = request.GET.get('page')
            try:
                fmsgs = paginator.page(page)
            except PageNotAnInteger:
                fmsgs = paginator.page(1)
            except EmptyPage:
                fmsgs = paginator.page(paginator.num_pages)
        else:
            fmsgs = None
        return render_to_response('quaba/messages/friendbox.html', 
                                    {'messages': fmsgs, 'friend': to_user},
                                    context_instance=RequestContext(request))
   
@csrf_protect  
@login_required(login_url='/quaba/login/')                       
def message_report(request, message_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    message = Message.objects.get_message(message_id)
    if 'report' in request.POST:
        message.report()
        msg = 'Has reportado un mensaje de %(name)s %(surname)s' % {'name': message.send_from.first_name,
                                                                    'surname': message.send_from.last_name}
        user.send_notification(msg)
        return HttpResponseRedirect(reverse('main_menu'))
    elif 'no_report' in request.POST:
        return HttpResponseRedirect(reverse('messages_inbox'))
    return render(request, 'quaba/messages/report.html')