from django.db.models import Q
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext as _
from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages 
from django.core.urlresolvers import reverse

from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

from quabamv.models import *
from quabamv.forms import *

@csrf_protect  
@login_required(login_url='/quaba/login/')
def photos_menu(request):
    return render(request,'quaba/photos/menu.html')
    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def photobook_view(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook,pbx = UserPhotoBook.objects.get_or_create(user=user)
    photo_list = UserPhoto.objects.photos_for_user(photobook)
    if photo_list:
        paginator = Paginator(photo_list, 10)
        page = request.GET.get('page')
        try:
            photos = paginator.page(page)
        except PageNotAnInteger:
            photos = paginator.page(1)
        except EmptyPage:
            photos = paginator.page(paginator.num_pages)
    else:
        photos = None
    return render_to_response('quaba/photos/photobook_view.html',
                                 {'photos': photos, 'user': user},
                                 context_instance=RequestContext(request))
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def filter_photos(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook,pbx = UserPhotoBook.objects.get_or_create(user=user)
    if 'year' in request.GET and 'month' in request.GET:
        year = request.GET['year']
        month = request.GET['month']
        photo_list = UserPhoto.objects.photos_by_date(photobook, year, month)  
        if photo_list:
            paginator = Paginator(photo_list, 10)
            page = request.GET.get('page')
            try:
                photos = paginator.page(page)
            except PageNotAnInteger:
                photos = paginator.page(1)
            except EmptyPage:
                photos = paginator.page(paginator.num_pages)
        else:
            photos = None
        return render_to_response('quaba/search/search_photo_results.html',
                                    {'photos': photos, 'friend': None, 'group': None,
                                    'date': Misc.objects.date_name(year, month)},
                                    context_instance=RequestContext(request))
    return render(request, 'quaba/search/search_photo_form.html')
                                                                                                                                        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def photo_view(request, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook, c = UserPhotoBook.objects.get_or_create(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    comm = UserPhotoComment.objects.filter(photo=photo)
    if comm:
        paginator = Paginator(comm, 10)
        page = request.GET.get('page')
        try:
            all_comments = paginator.page(page)
        except PageNotAnInteger:
            all_comments = paginator.page(1)
        except EmptyPage:
            all_comments = paginator.page(paginator.num_pages)
    else:
        all_comments = None
    notifications = Notification.objects.all_notifications(user)
    if notifications:
        for n in notifications:
            if n.type == settings.VIEW_USER_PHOTO and n.parameter_id == photo.id:
                n.delete()
    if photo.creator.email != user.email:
        pcr = False
    else:
        pcr = True
    return render_to_response('quaba/photos/photo_view.html',
                                 {'photo': photo, 'pcr': pcr, 'comm': all_comments},
                                 context_instance=RequestContext(request))
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def friend_photobook_view(request, email):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(from_user, to_user):
        photobook,pbx = UserPhotoBook.objects.get_or_create(user=to_user)
        photo_list = UserPhoto.objects.photos_for_user(photobook)
        if photo_list:
            paginator = Paginator(photo_list, 10)
            page = request.GET.get('page')
            try:
                photos = paginator.page(page)
            except PageNotAnInteger:
                photos = paginator.page(1)
            except EmptyPage:
                photos = paginator.page(paginator.num_pages)
        else:   
            photos = None
        return render_to_response('quaba/photos/friend_photobook_view.html',
                                    {'photos': photos, 'user': to_user},
                                    context_instance=RequestContext(request))
                                    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def friend_filter_photos(request, email):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(from_user, to_user):
        photobook,pbx = UserPhotoBook.objects.get_or_create(user=to_user)
        if 'year' in request.GET and 'month' in request.GET:
            year = request.GET['year']
            month = request.GET['month']
            photo_list = UserPhoto.objects.photos_by_date(photobook, year, month)  
            if photo_list:
                paginator = Paginator(photo_list, 10)
                page = request.GET.get('page')
                try:
                    photos = paginator.page(page)
                except PageNotAnInteger:
                    photos = paginator.page(1)
                except EmptyPage:
                    photos = paginator.page(paginator.num_pages)
            else:
                photos = None
            return render_to_response('quaba/search/search_photo_results.html',
                                        {'photos': photos, 'friend': to_user, 'group': None,
                                        'date': Misc.objects.date_name(year, month)},
                                        context_instance=RequestContext(request))
    return render(request, 'quaba/search/search_photo_form.html')    
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def friend_photo_view(request, email, photo_id):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)
    if Friendship.objects.are_friends(from_user, to_user):
        notifications = Notification.objects.all_notifications(from_user)
        photobook, c = UserPhotoBook.objects.get_or_create(user=to_user)
        photo = UserPhoto.objects.get_photo(photobook, photo_id)
        comm = UserPhotoComment.objects.filter(photo=photo)
        
        if notifications:
            for n in notifications:
                if n.type == settings.VIEW_USER_PHOTO and n.parameter_id == photo.id:
                    n.delete()
        
        if comm:
            paginator = Paginator(comm, 10)
            page = request.GET.get('page')
            try:
                all_comments = paginator.page(page)
            except PageNotAnInteger:
                all_comments = paginator.page(1)
            except EmptyPage:
                all_comments = paginator.page(paginator.num_pages)
        else:
            all_comments = None
            
        if photo.creator.email != from_user.email:
            pcr = False
        else:
            pcr = True
        return render_to_response('quaba/photos/friend_photo_view.html',
                                 {'photo': photo, 'pcr': pcr, 'comm': all_comments, 'me': from_user, 'user': to_user},
                                 context_instance=RequestContext(request)) 
    return HttpResponseRedirect(reverse('main_menu'))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def photo_report(request, email, photo_id):
    from_user = UserProfile.objects.get_profile_user(request.user.email)
    to_user = UserProfile.objects.get_profile_user(email)  
    if Friendship.objects.are_friends(from_user, to_user):
        photobook, c = UserPhotoBook.objects.get_or_create(user=to_user)
        photo = UserPhoto.objects.get_photo(photobook, photo_id)
        if photo.creator.email != from_user.email:
            if 'report' in request.POST:
                photo.report()
                msg = 'Has reportado una foto de %(name)s %(surname)s' % {'name': to_user.first_name,
                                                                       'surname': to_user.last_name} 
                from_user.send_notification(msg)
                return HttpResponseRedirect(reverse('main_menu'))
            elif 'no_report' in request.POST:
                return HttpResponseRedirect(reverse('friend_photo_view', args=(email, photo.id,)))
            return render(request, 'quaba/photos/report.html')
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def upload_photo(request):
    user = UserProfile.objects.get_profile_user(request.user.email)
    if request.method == 'POST':
        form = UserPhotoForm(request.POST, request.FILES)
        if form.is_valid():
            photo = form.save(user)
            user.send_notification('Foto subida con exito', settings.VIEW_USER_PHOTO, photo.id)
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,))) 
    else:
        form = UserPhotoForm()
    return render_to_response('quaba/photos/photo_upload.html',
                                 {'form': form},
                                 context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def rename_photo(request, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook = UserPhotoBook.objects.get(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    if photo.creator.email == user.email:
        if request.method == 'POST':
            form = TitleForm(request.POST)
            if form.is_valid():
                old_name = photo.title
                new_name = form.cleaned_data['title']
                if new_name != old_name:
                    photo.title = new_name
                    photo.save() 
                    user.send_notification('Foto renombrada con exito')
                    return HttpResponseRedirect(reverse('photo_view', args=(photo_id,))) 
                else:
                    return HttpResponseRedirect(reverse('rename_photo', args=(photo_id,))) 
        else:
            form = TitleForm(initial={'title': photo.title})
        return render_to_response('quaba/photos/photo_rename.html',
                              {'form': form, 'photo': photo},
                              context_instance=RequestContext(request))
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def user_main_photo(request, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook, cr = UserPhotoBook.objects.get_or_create(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    if photo.creator.email == user.email:
        ai,aix = UserAdditionalInfo.objects.get_or_create(user=user)
        ai.profile_photo = photo
        ai.save()
        user.send_notification('Foto principal modificada con exito')
        return HttpResponseRedirect(reverse('main_menu'))
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def delete_photo(request, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook, x = UserPhotoBook.objects.get_or_create(user=user)
    photo = UserPhoto.objects.get(id=photo_id)
    if user.email == photo.creator.email:
        if 'delete' in request.POST:
            friends = Friendship.objects.friends_for_user(user)
            for friend in friends:
                fpbook, fx = UserPhotoBook.objects.get_or_create(user=friend)
                p = UserPhoto.objects.get_shared_photo(user, fpbook, photo_id)
                if p:
                    notifications = Notification.objects.all_notifications(friend)
                    if notifications:
                        for n in notifications:
                            if n.type == settings.VIEW_USER_PHOTO and n.parameter_id == p.id:
                                n.delete()
                    p.delete()
            ai,aix = UserAdditionalInfo.objects.get_or_create(user=user)
            if not ai.profile_photo or ai.profile_photo.id != photo.id:
                photo.delete()
                user.send_notification('Foto eliminada con exito')
            else:
                user.send_notification('Esta foto es seleccionada como tu foto de perfil y no se puede eliminar')
            return HttpResponseRedirect(reverse('photobook_view')) 
        elif 'no_delete' in request.POST:
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,))) 
        return render(request, 'quaba/photos/photo_delete.html')
                              
@csrf_protect  
@login_required(login_url='/quaba/login/')
def share_photo(request, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook = UserPhotoBook.objects.get(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    
    us = UserProfile.objects.all()
    us = us.exclude(email=user.email)
    
    for u in us:
        upbook, xy = UserPhotoBook.objects.get_or_create(user=u)
        if not Friendship.objects.are_friends(u, user) or UserPhoto.objects.get_shared_photo(user, upbook, photo.id):
            us = us.exclude(email=u.email)
    if 'q' in request.GET:
        q = request.GET['q']
        if q:
            for term in q.split():
                us = us.filter(Q(first_name__icontains=term)|Q(last_name__icontains=term))  
        
    if request.method == 'POST':
        form = ShareFriendForm(user, us, request.POST)
        if form.is_valid():
            friends = form.cleaned_data['to_users']
            for friend in friends:
                fpbook, xx = UserPhotoBook.objects.get_or_create(user=friend)
                if UserPhoto.objects.get_shared_photo(user, fpbook, photo.id):
                    user_message = "Ya has compartido esta foto con %s %s" % {friend.first_name, friend.last_name} 
                else:    
                    shp = photo.share(friend)
                    user_message = "Foto compartida con %(name)s %(surname)s con exito" % {'name': friend.first_name, 
                                                                                            'surname': friend.last_name} 
                    friend_message = "%(name)s %(surname)s ha compartido una foto contigo." % {'name': user.first_name,
                                                                                               'surname': user.last_name}
                    friend.send_notification(friend_message, settings.VIEW_USER_PHOTO, shp.id)
                user.send_notification(user_message)
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,))) 
    else:
        form = ShareFriendForm(user, us)   
    return render_to_response('quaba/photos/photo_share.html',
                                    {'form': form, 'photo': photo, 'friends': us},
                                    context_instance=RequestContext(request))

@csrf_protect  
@login_required(login_url='/quaba/login/')
def delete_shared_photo(request, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook = UserPhotoBook.objects.get(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    if user.email != photo.creator.email:
        if 'delete' in request.POST:
            photo.delete()
            user.send_notification('Foto eliminada con exito')
            return HttpResponseRedirect(reverse('photobook_view')) 
        elif 'no_delete' in request.POST:
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,))) 
        return render(request, 'quaba/photos/photo_delete.html')

@csrf_protect  
@login_required(login_url='/quaba/login/')
def like_photo(request, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook = UserPhotoBook.objects.get(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    pv, pvx = UserPhotoVote.objects.get_or_create(user=user, photo=photo)
    if pv.voted:
        return HttpResponseRedirect(reverse('photo_view', args=(photo.id,))) 
    else:
        pv.set_like() 
        if user.email != photo.creator.email:
            msg = "%(name)s %(surname)s ha comentado una foto" % {'name': user.first_name, 'surname': user.last_name}
            friends = Friendship.objects.friends_for_user(user)
            for friend in friends:
                fpbook, fx = UserPhotoBook.objects.get_or_create(user=friend)
                p = UserPhoto.objects.get_shared_photo(photo.creator, fpbook, photo_id)
                if p:
                    friend.send_notification(msg, settings.VIEW_USER_PHOTO, p.id)
            p = UserPhoto.objects.get_shared_photo(photo.creator, photobook, photo_id)
            message = "A %(name)s %(surname)s le gusta una de tus fotos" % {'name': user.first_name,
                                                                    'surname': user.last_name}                                                      
            photo.creator.send_notification(message, settings.VIEW_USER_PHOTO, p.id) 
        return HttpResponseRedirect(reverse('photo_view', args=(photo_id,))) 
        
@csrf_protect  
@login_required(login_url='/quaba/login/')
def photo_likes_list(request, photo_id):
    photo = UserPhoto.objects.get(id=photo_id)
    pvs = UserPhotoVote.objects.filter(photo=photo)   
    like_users = []
    if pvs:
        for pv in pvs:
            like_users.append(pv.user)
    if like_users:
        paginator = Paginator(like_users, 10)
        page = request.GET.get('page')
        try:
            likes = paginator.page(page)
        except PageNotAnInteger:
            likes = paginator.page(1)
        except EmptyPage:
            likes = paginator.page(paginator.num_pages)
    else:
        likes = None
    return render(request, 'quaba/photos/likes.html', {'likes': likes})
                                    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def photo_new_comment(request, photo_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook = UserPhotoBook.objects.get(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            c = form.save(user)
            comment = UserPhotoComment(photo=photo, comment=c)
            comment.save()
            
            numc, numcx = UserAllComments.objects.get_or_create(user=user)
            numc.inc()
            
            msg = "%(name)s %(surname)s ha comentado una foto" % {'name': user.first_name, 'surname': user.last_name}
            friends = Friendship.objects.friends_for_user(user)
            for friend in friends:
                fpbook, fx = UserPhotoBook.objects.get_or_create(user=friend)
                p = UserPhoto.objects.get_shared_photo(user, fpbook, photo_id)
                if p:
                    friend.send_notification(msg, settings.VIEW_USER_PHOTO, p.id)
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,)))
    else:
        form = CommentForm()
    return render_to_response('quaba/photos/new_comment.html',
                                {'form': form, 'photo': photo},
                                context_instance=RequestContext(request)) 
                                                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def photo_delete_comment(request, photo_id, comment_id): 
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook = UserPhotoBook.objects.get(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    comm = UserPhotoComment.objects.get(photo=photo, id=comment_id)
    if photo.creator.email == user.email or comm.comment.author.email == user.email:
        if 'delete' in request.POST:
            comm.delete()
            user.send_notification('Comentario eliminado con exito')
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,)))
        elif 'no_delete' in request.POST:
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,)))
    return render(request, 'quaba/photos/remove_comment.html') 
    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def photo_comment_report(request, photo_id, comment_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook = UserPhotoBook.objects.get(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    c = UserPhotoComment.objects.get(photo=photo, id=comment_id)
    if c.comment.author.email != user.email:
        if 'report' in request.POST:
            c.report()
            msg = 'Has reportado un comentario de %(name)s %(surname)s' % {'name': c.comment.author.first_name,
                                                                       'surname': c.comment.author.last_name} 
            from_user.send_notification(msg)
            return HttpResponseRedirect(reverse('main_menu'))
        elif 'no_report' in request.POST:
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,)))
        return render(request, 'quaba/photos/comment_report.html')
    
@csrf_protect  
@login_required(login_url='/quaba/login/')
def photo_like_comment(request, photo_id, comment_id): 
    user = UserProfile.objects.get_profile_user(request.user.email)
    photobook = UserPhotoBook.objects.get(user=user)
    photo = UserPhoto.objects.get_photo(photobook, photo_id)
    c = UserPhotoComment.objects.get(photo=photo, id=comment_id)
    if c:
        cv, cvx = UserPhotoCommentVote.objects.get_or_create(user=user, comment=c)
        if cv.voted:
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,)))
        else:
            cv.set_like()
            if c.comment.author.email != user.email:
                msg = "A %(name)s %(surname)s le gusta un comentario en esta foto" % {'name': user.first_name, 'surname': user.last_name}
                friends = Friendship.objects.friends_for_user(user)
                for friend in friends:
                    fpbook, fx = UserPhotoBook.objects.get_or_create(user=friend)
                    p = UserPhoto.objects.get_shared_photo(user, fpbook, photo_id)
                    if p:
                        friend.send_notification(msg, settings.VIEW_USER_PHOTO, p.id)
                like_message = "A %(name)s %(surname)s le gusta un comentario tuyo en %(title)s" % {'name': user.first_name,
                                                                                                    'surname': user.last_name,
                                                                                                    'title': photo.title}
                c.comment.author.send_notification(like_message, settings.VIEW_USER_PHOTO, shp.id) 
            return HttpResponseRedirect(reverse('photo_view', args=(photo.id,)))
            
@csrf_protect  
@login_required(login_url='/quaba/login/')
def photo_comment_likes_list(request, photo_id, comment_id):
    photo = UserPhoto.objects.get(id=photo_id)
    c = UserPhotoComment.objects.get(photo=photo, id=comment_id)
    cvs = UserPhotoCommentVote.objects.filter(comment=c)
    like_users = []
    if cvs:
        for cv in cvs:
            like_users.append(cv.user)
    if like_users:
        paginator = Paginator(like_users, 10)
        page = request.GET.get('page')
        try:
            likes = paginator.page(page)
        except PageNotAnInteger:
            likes = paginator.page(1)
        except EmptyPage:
            likes = paginator.page(paginator.num_pages)
    else:
        likes = None
    return render(request, 'quaba/photos/comment_likes.html', {'likes': likes})
        