from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect

@csrf_protect                                                            
def handler404(request):
    response = render_to_response('quaba/error/404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response

@csrf_protect                                                            
def handler500(request):
    response = render_to_response('quaba/error/500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response