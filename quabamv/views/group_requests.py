from django.db.models import Q
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.conf import settings

from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect

from quabamv.models import *
from quabamv.forms import *

@csrf_protect  
@login_required(login_url='/quaba/login/')
def group_send_request(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    if not Membership.objects.is_member(user, group):
        done = False
        if 'join_request' in request.POST:
            req = MembershipRequest(group=group,from_user=user)
            req.save() 
            done = True            
            msg = 'Solicitud enviada a %(group)s' % {'group': group.name}
            user.send_notification(msg)
            join_message = "%(name)s %(surname)s quiere unirse a %(groupname)s" % {'name': user.first_name,
                                                                               'surname': user.last_name,
                                                                               'groupname': group.name}                                                                
            group.send_request_notification(join_message, user.email)
            return HttpResponseRedirect(reverse('groups_menu'))
        return render_to_response('quaba/groups/requests/send_request.html',
                                {'group': group, 'done': done},
                                context_instance=RequestContext(request))
                                 
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_group_received_requests(request, group_id): 
    group = FriendsGroup.objects.get_group(group_id)
    req_list = MembershipRequest.objects.get_requests_to_group(group)
    if req_list:
        paginator = Paginator(req_list, 10)
        page = request.GET.get('page')
        try:
            reqs = paginator.page(page)
        except PageNotAnInteger:
            reqs = paginator.page(1)
        except EmptyPage:
            reqs = paginator.page(paginator.num_pages)
    else:
        reqs = None
    return render_to_response('quaba/groups/requests/requests_group_list.html',
                                {'group': group, 'reqs': reqs},
                                context_instance=RequestContext(request))
                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def list_group_sent_requests(request): 
    user = UserProfile.objects.get_profile_user(request.user.email)
    req_list = MembershipRequest.objects.get_requests_from_member(user)
    if req_list:
        paginator = Paginator(req_list, 10)
        page = request.GET.get('page')
        try:
            reqs = paginator.page(page)
        except PageNotAnInteger:
            reqs = paginator.page(1)
        except EmptyPage:
            reqs = paginator.page(paginator.num_pages)
    else:
        reqs = None
    return render_to_response('quaba/groups/requests/requests_user_list.html',
                                {'reqs': reqs},
                                context_instance=RequestContext(request))
                                
@csrf_protect  
@login_required(login_url='/quaba/login/')
def delete_group_request(request, group_id):
    user = UserProfile.objects.get_profile_user(request.user.email)
    group = FriendsGroup.objects.get_group(group_id)
    req = MembershipRequest.objects.get_request_member_group(group, user) 
    if 'delete' in request.POST:
        req.delete()
        msg = 'Se ha eliminado tu solicitud para unirte a %(group)s' % {'group': group.name}
        user.send_notification(msg)
        return HttpResponseRedirect(reverse('groups_menu'))
    elif 'no_delete' in request.POST:
        return HttpResponseRedirect(reverse('groups_sent_requests'))
    return render(request, 'quaba/groups/requests/request_delete.html', {'req': req})
      
@csrf_protect  
@login_required(login_url='/quaba/login/')
def respond_to_group_request(request, group_id, email):
    user = UserProfile.objects.get_profile_user(request.user.email)
    rquser = UserProfile.objects.get_profile_user(email)
    group = FriendsGroup.objects.get_group(group_id)

    if group.creator.email == user.email:
        ns = GroupNotification.objects.all_notifications(group, user)
        req = MembershipRequest.objects.get_request_member_group(group, rquser)
        if ns:
            for n in ns:
                if n.type == settings.VIEW_GROUP_REQUEST and n.group.id == group.id and n.parameter_id == req.id:
                    n.delete()
        if 'accept' in request.POST:
            req.accept() 
            msg = '%(name)s %(surname)s se ha unido a %(groupname)s' % {'name': rquser.first_name, 
                                                                        'surname': rquser.last_name, 
                                                                        'groupname': group.name}
            group.send_notification(msg, settings.VIEW_GROUP_MEMBERS)
            join_msg = 'Has sido aceptado en el grupo %(gname)s'
            rquser.send_notification(msg, settings.VIEW_GROUP, group.id)
            return HttpResponseRedirect(reverse('list_group_members', args=(group.id,)))
        elif 'decline' in request.POST:
            req.decline()
            return HttpResponseRedirect(reverse('group_view', args=(group.id,)))
        return render(request, 'quaba/groups/requests/request_group_show.html', {'req': req})