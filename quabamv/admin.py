from django.conf import settings
from django.contrib import admin

from models import *

admin.site.register(UserProfile)
admin.site.register(Message)
admin.site.register(FriendsGroup)
admin.site.register(UserPhoto)
admin.site.register(UserFolder)
admin.site.register(UserFile)
